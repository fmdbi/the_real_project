package beans.geometry;

/**
 * Created by Mika on 09.11.2016.
 */
public class Kreis
{
    private Koordinaten midPoint;
    private double radius;
    private Koordinaten normalVector;

    /**
     * Defaultkonstruktor <br><br>
     *
     * midPoint = (0, 0, 0) <br>
     * radius = 0
     */
    public Kreis()
    {
        this.midPoint = new Koordinaten();
        this.radius = 0;
        this.normalVector = new Koordinaten();
    }

    /**
     * Konstruktor
     *
     * @param radius
     * @param midPoint
     */
    public Kreis(double radius, Koordinaten midPoint, Koordinaten normalVector)
    {
        this.radius = radius;
        this.midPoint = midPoint;
        this.normalVector = normalVector;
    }

    public Koordinaten getMidPoint()
    {
        return this.midPoint;
    }

    public void setMidPoint(Koordinaten mid)
    {
        this.midPoint = mid;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof Kreis)
        {
            Kreis circle = (Kreis) obj;

            if(this.midPoint.equals(circle.midPoint))
            {
                return this.radius == circle.getRadius();
            }
            return false;
        }
        return false;
    }

    public double getRadius()
    {
        return this.radius;
    }

    public void setRadius(double radius)
    {
        this.radius = radius;
    }

    public String toString()
    {
        return "Mittelpunkt: " + this.midPoint + " Radius: " + getRadius() + " Normalenvektor: " + this.getNormalVector();
    }

    public Koordinaten getNormalVector()
    {
        return this.normalVector;
    }
}
