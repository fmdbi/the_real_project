package beans.geometry;

import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Random;

public class Koordinaten
{
	private static final Logger logger = Logger.getLogger(Koordinaten.class);

	private static final int SQUARE = 2;
	private static final BigDecimal TWO = new BigDecimal(2);

	private double x;
	private double y;
	private double z;
	
	public Koordinaten()
	{
		this.x = 0;
		this.y = 0;
		this.z = 0;
	}
	
	public Koordinaten(double x, double y, double z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public double calcDistance(Koordinaten target)
	{
		BigDecimal distX = (new BigDecimal(target.getX()).subtract(new BigDecimal(this.getX()))).abs(); // (xTarget - xSource).abs()
		BigDecimal distY = (new BigDecimal(target.getY()).subtract(new BigDecimal(this.getY()))).abs(); // (yTarget - ySource).abs()
		BigDecimal distZ = (new BigDecimal(target.getZ()).subtract(new BigDecimal(this.getZ()))).abs(); // (zTarget - zSource).abs()

		BigDecimal sqDistX = distX.pow(SQUARE);
		BigDecimal sqDistY = distY.pow(SQUARE);
		BigDecimal sqDistZ = distZ.pow(SQUARE);

		return sqrt(sqDistX.add(sqDistY, new MathContext(10, RoundingMode.HALF_UP)).add(sqDistZ, new MathContext(10, RoundingMode.HALF_UP)), 10).doubleValue();
	}

    public double getX()
    {
        return this.x;
    }

    public void setX(double x)
    {
        this.x = x;
    }

	/*
	 *
	 * 			GETTER / SETTER
	 *
	 */

    public double getY()
    {
        return this.y;
    }

	public void setY(double y)
	{
		this.y = y;
	}

    public double getZ()
    {
        return this.z;
    }

    public void setZ(double z)
    {
        this.z = z;
    }

    /*
     *
      * 			Methoden
      *
      */
    private static BigDecimal sqrt(BigDecimal A, final int SCALE)
    {
        BigDecimal x0 = new BigDecimal("0");
        BigDecimal x1 = new BigDecimal(Math.sqrt(A.doubleValue()));
        while (!x0.equals(x1))
        {
            x0 = x1;
            x1 = A.divide(x0, SCALE, RoundingMode.HALF_UP);
            x1 = x1.add(x0);
            x1 = x1.divide(TWO, SCALE, RoundingMode.HALF_UP);
        }
        return x1;
    }

    public void randomize(int intervalBorder)
    {
        double min = -0.5 * intervalBorder;
        double max = 0.5 * intervalBorder;
        Random random = new Random();

        this.x = Math.random() * -0.5 * min + (max - min) * random.nextDouble();
        this.y = Math.random() * -0.5 * min + (max - min) * random.nextDouble();
        this.z = Math.random() * -0.5 * min + (max - min) * random.nextDouble();
    }

	/*
	 *
	 * 				equals() / toString()
	 *
	 */

	/**
	 *
	 * @param obj
	 * @return
	 */
	@Override
	public boolean equals(Object obj)
	{
		if(obj instanceof Koordinaten)
		{
			if(this.x == ((Koordinaten) obj).getX())
			{
				if(this.y == ((Koordinaten) obj).getY())
				{
                    // falsche Z-Koordinate
                    return this.z == ((Koordinaten) obj).getZ();
                }
				// falsche Y-Koordinate
				return false;
			}
			// falsche X-Koordinate
			// return false;
		}
		// falsche Instanz
		return false;
	}

	public String toString()
	{
		return this.x + "\t" + this.y + "\t" + this.z;
	}
}
