package beans.chem;

import beans.geometry.Koordinaten;
import org.apache.log4j.Logger;

public class Atom 
{
	private static final Logger logger = Logger.getLogger(Atom.class);
	
	private int idAtom;
	private Koordinaten koordinaten;
	private String element;

	/**
	 * Defaultkonstruktor
	 */
	public Atom()
	{
		this.idAtom = 0;
		this.koordinaten = new Koordinaten();
	}
	
	/**
	 * Konstruktor ohne Atom-Liste
	 * 
	 * @param id ID des Atoms
	 * @param element Kuerzel Elementname
	 */
	public Atom(int id, String element, Koordinaten koordinaten)
	{
		this.idAtom = id;
		this.koordinaten = koordinaten;
		this.element = element;
	}

	/*-----------------------------------------------
	 *				Getter / Setter
	 ----------------------------------------------*/
	
	/**
	 * Setter Koordinaten
	 * @param koords
	 */
	public void setKoordinaten(Koordinaten koords)
	{
		this.koordinaten = koords;
	}
	
	/**
	 *
	 * @return
	 */
	@Override
	public boolean equals(Object obj)
	{
		if(obj instanceof Atom)
		{
			if(this.getElement().equals(((Atom) obj).getElement()))
			{
				if(this.getIdAtom() == ((Atom) obj).getIdAtom())
				{
                    // falsche Koordinate
                    return this.getCoordinates().equals(((Atom) obj).getCoordinates());
                }
				else
				{
					// falsche ID
					return false;
				}
			}
			else
			{
				// falsches Element
				return false;
			}
		}
		else
		{
			// falsche Instanz
			return false;
		}
    }

    /**
     * Getter Kurzform des chem. Elements
     *
     * @return
     */
    public String getElement()
    {
        return this.element;
    }

    /**
     * Getter Atom-ID
     *
     * @return
     */
    public int getIdAtom()
    {
        return this.idAtom;
    }

    public Koordinaten getCoordinates()
    {
        return this.koordinaten;
    }

    /**
     * Setter Atom-ID
     *
     * @param id
     */
    public void setIdAtom(int id)
    {
        this.idAtom = id;
    }

    /**
     * Setter Kurzform chem. Element
     *
     * @param element
     */
    public void setElement(String element)
    {
        this.element = element;
    }

	public String toString()
	{
		return "Id: " + this.idAtom + " Element: " + this.element;
	}
}
