package beans.graph;

import org.apache.log4j.Logger;

/**
 * Edge-Klasse
 *
 * @author Mika
 */
public class Edge extends GraphElement
{
    private static final Logger logger = Logger.getLogger(Edge.class);
    public static int count = 0;

    private int idEdge;
    private double length = 0;
    private boolean pseudo = false;
    private boolean molecularConjecture = false;
    private boolean covered = false;
    private boolean redundant = false;

    /**
     * Defaultkonstruktor
     */
    public Edge()
    {
        this.idEdge = count++;
    }

    /**
     * Konstruktor für Edges
     *
     * @param pseudo Ist die Edge temporär (durch Vervielfachung entstanden)?
     */
    public Edge(boolean pseudo, boolean molecularConjecture)
    {
        this.idEdge = count++;
        this.pseudo = pseudo;
        this.molecularConjecture = molecularConjecture;
    }

    /**
     * Konstruktor Edges
     *
     * @param id
     * @param pseudo Ist die Edge temporär (durch Vervierfachung entstanden)?
     */
    public Edge(int id, double length, boolean pseudo, boolean molecularConjecture)
    {
        count++;
        this.idEdge = id;
        this.length = length;
        this.pseudo = pseudo;
        this.molecularConjecture = molecularConjecture;
    }

    /*----------------------------------------------------------------------------------------
     *
     *                                PEBBLE-Methoden
     *
     ----------------------------------------------------------------------------------------*/

    /**
     * Richtet Edge von pebbleDonator weg,
     * berücksichtigt Pebbles
     *
     * (Edge-Covering: Mid-Level)
     *
     * @param pebbleDonator Node, von dem die Edge weg-gerichtet werden soll
     * @throws Exception Bei nicht-verbundenen Nodes bzw. Edges
     */
    void orientAway(Graph graph, Node pebbleDonator) throws Exception
    {
        if (pebbleDonator == graph.getStartNode(this)) // Bereits korrekt gerichtet,
        {
            if (!this.isCovered()) // aber Gerichtet-Flag fehlt noch
            {
                try
                {
                    graph.getStartNode(this).decrPebbles();
                    this.covered = true;
                }
                catch (Exception e)
                {
                    throw new Exception("No pebble available to cover edge");
                }
            }
        }
        else if (pebbleDonator == graph.getEndNode(this)) // Edge muss umgedreht werden
        {
            try
            {
                graph.getEndNode(this).decrPebbles();
                if (this.isCovered()) // Edge war vorher schon gerichtet => reines Umdrehen
                {
                    graph.getStartNode(this).incrPebbles(); // Gib dem (zukünftigen) End-Node dessen Pebble zurück
                }
                graph.changeDirection(this);
                this.covered = true;
            }
            catch (Exception e)
            {
                throw new Exception("No pebble available to cover edge");
            }
        }
        else
        {
            throw new Exception("Given node is not connected to edge");
        }
    }


    /*----------------------------------------------------------------------------------------
     *
     *                                SETTER- und GETTER-Methoden
     *
     --------------------------------------------------------------------------------------*/

    /**
     * Ist diese Edge gecovert (gerichtet)?
     */
    public boolean isCovered()
    {
        return this.covered;
    }

    /**
     * Ist diese Edge gecovert (gerichtet)?
     */
    public void setCovered(boolean covered)
    {
        this.covered = covered;
    }

    /**
     * Wird diese Edge vom angegebenen Node gecovert?
     *
     * @param node Node, dessen Cover überprüft werden soll
     */
    public boolean isCoveredBy(Graph graph, Node node)
    {
        return this.isCovered() && graph.getStartNode(this) == node;
    }

    /**
     * Getter Id-Suffix
     *
     * @return Den Id der Edge
     */
    public int getIdEdge()
    {
        return this.idEdge;
    }

    /**
     * Getter Laenge
     *
     * @return length
     */
    public double getLength()
    {
        return this.length;
    }

    /**
     *
     * @return
     */
    public String toString()
    {
        return "Id: " + this.idEdge + " Length: " + this.length;
    }

    /**
     * Ist diese Edge eine Pseudoedge (durch Vervierfachen entstanden)?
     */
    public boolean isPseudo()
    {
        return this.pseudo;
    }

    public boolean isRedundant()
    {
        return this.redundant;
    }

    /**
     * Setter redundancy
     *
     * @param redundancy
     */
    public void setRedundant(boolean redundancy)
    {
        this.redundant = redundancy;
    }

    public boolean isMolecularConjecture()
    {
        return this.molecularConjecture;
    }

}
