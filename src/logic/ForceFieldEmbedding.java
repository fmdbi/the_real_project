/**
 * Autor: @alex
 *
 * FF-Embedder als simples Modell gebaut. Anziehende und abstoßende Kraefte werden betrachtet, allerdings
 * werden anziehnde Kräfte nur angewendet, wenn die gegebenen Abstände zwischen den Atomen nicht überschritten
 * ocer erreicht wurden. Abstoßende Kräfte werden angepasst, je nachdem wie viele Edges schon die Länge der
 * Bindung haben oder drunter bzw. drüber liegen.
 */

package logic;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import org.apache.log4j.Logger;

import beans.geometry.Koordinaten;
import beans.graph.Edge;
import beans.graph.Graph;
import beans.graph.Node;

public class ForceFieldEmbedding
{
    private static final Logger logger = Logger.getLogger(ForceFieldEmbedding.class);

    /**
     * C, K: Konstanten für den FF-Embedder, entnommen aus
     * http://www.mathematica-journal.com/issue/v10i1/contents/graph_draw/graph_draw.pdf.
     * anneal: dient der Anpassung der Kraftvektoren, um diese langsam an die richtige Laenge anzupassen.
     * under, over, right: Anzahl der Edges, die unter, ueber der gegebenen Laenge liegen oder genau richtig
     * sind. Richtige Edges werden nicht mehr angepasst.
     */
    private static double C = 0.2;
    private static double K = 1.2;
    private static double anneal=1.0;
    private static double COOLINGFACTOR=0.9;
    private static int right=0;
    private static int under=0;
    private static int over=0;
    private static int count=0;

    public static void ForceByLength (Graph graph, int edgeFactor) {

        HashSet<Node> nodes = new HashSet<Node>();
        nodes=graph.getSetNodes();

        double[] forceVector;

        double distTotalOld=10;
        double distTmp=0;
        double distUnder = 0;
        double distOver = 0;
        double distTotal=1;

        for (Node node : nodes)
        {
            node.setCoordinates(new Koordinaten());
            node.getCoordinates().randomize(nodes.size() * 2);
        }

        int durchlauf=0;
        //for (Node node:graph.getSetNodes()) {
        //	System.out.println(node.getCoordinates().getX()+","+node.getCoordinates().getY()+","+node.getCoordinates().getZ());
        //}


        while (!(right== (graph.getSetEdges().size() *2) && anneal<=0.0001)) {

            right=0;
            under=0;
            over=0;

            distTmp=0;
            distTotal=0;

            /**
             * RANDOM Durchlauf, damit die Reihenfolge der Anpassung unterschiedlich ist und so der
             * FF-Embedder moeglichst nicht in einer Schleife gefangen ist, in der Knoten nur leicht
             * verschoben werden und womoeglich "im Kreis laufen".
             */
            ArrayList<Node> rndnodes = new ArrayList<Node>(nodes);
            Collections.shuffle(rndnodes);
            distUnder = 0;
            distOver = 0;

            for (Node node:rndnodes) {
                distTmp=0;
                // System.out.println(graph.getSetEdges(node).size());
                for (Edge itedge:graph.getSetEdges(node)) {
                    Koordinaten startnode=node.getCoordinates();
                    Koordinaten endnode=getPartnerKoordinaten(node, itedge, graph).getCoordinates();
                    if (round(startnode.calcDistance(endnode),5)==round(itedge.getLength(),5)) {
                        right++;
                        //System.out.println("RIGHT!");
                    } else if (round(startnode.calcDistance(endnode),5)<round(itedge.getLength(),5)) {
                        under++;
                        distUnder += Math.abs(startnode.calcDistance(endnode) - itedge.getLength());
                        //System.out.println("UNDER!");
                    } else if (round(startnode.calcDistance(endnode),5)>round(itedge.getLength(),5)) {
                        over++;
                        distOver += Math.abs(startnode.calcDistance(endnode) - itedge.getLength());
                        //System.out.println("OVER!");
                    }
                    distTmp+=round((startnode.calcDistance(endnode)-itedge.getLength()),5);
                    //System.out.println(startnode.calcDistance(endnode)+"  "+itedge.getLength());
                }
                distTotal+=distTmp/edgeFactor;
            }

            System.out.println("Durchlauf "+durchlauf+": Anneal: "+anneal+" distTotal: "+distTotal+" distUnder: "+ distUnder +" distOver: "+ distOver +" right: "+right + " countEdges*2: " + graph.getSetEdges().size() *2);
            //System.out.println("Durchlauf "+durchlauf+": Anneal: "+anneal+" distTotal: "+distTotal+" under: "+under+" over: "+over+" right: "+right + " countEdges*2: " + graph.getSetEdges().size() *2);
            durchlauf++;

            /**
             * Berechnen des Annealing.
             */
            /*
            if (right==0) {
                if (over>0) {
                    anneal*=Math.pow(COOLINGFACTOR, over);
                }
            } else {
                if (distTotal>distTotalOld) {
                    anneal/=Math.pow(COOLINGFACTOR, under);
                } else {
                    anneal*=COOLINGFACTOR;
                }
            }
            */

            double facOver = 0;
            double facUnder = 0;

            if(over != 0)
            {
                facOver = distOver / over;
            }
            else
            {
                facOver = 1;
            }

            if(under != 0)
            {
                facUnder = distUnder / under;
            }
            else
            {
                facUnder = 1;
            }

            double korrektur = 0;
            if(facOver > facUnder)
            {
                // anneal kleiner werden
                korrektur = 1/(facOver / facUnder);
                anneal = (anneal - korrektur) * COOLINGFACTOR;

            }
            else if(facOver < facUnder)
            {
                // anneal größer werden
                korrektur = 1 / (facOver / facUnder);
                anneal = (anneal + korrektur) * COOLINGFACTOR;
            }
            else
            {
                anneal *= COOLINGFACTOR;
            }


            distTotalOld=distTotal;

            for (Node node:rndnodes) {

                // Berechnen der abstoßenden Kraft
                forceVector=forceRepelVector(node, nodes, graph);

                // Abstoßung + annealing
                node.getCoordinates().setX(node.getCoordinates().getX()+(forceVector[0]*anneal));
                node.getCoordinates().setY(node.getCoordinates().getY()+(forceVector[1]*anneal));
                node.getCoordinates().setZ(node.getCoordinates().getZ()+(forceVector[2]*anneal));

                // Anziehung + annealing
                // Erstellen eines "Partner"-Vektors, um nur die Endknoten einer Edge zu übergeben
                HashSet<Node> partner = new HashSet<Node>();
                for (Edge itedge:graph.getSetEdges(node)) {
                    partner.add(getPartnerKoordinaten(node, itedge, graph));
                }

                // Berechnen der anziehenden Kraft
                forceVector=forceAttVector(node, partner, graph);
                node.getCoordinates().setX(node.getCoordinates().getX()+forceVector[0]);
                node.getCoordinates().setY(node.getCoordinates().getY()+forceVector[1]);
                node.getCoordinates().setZ(node.getCoordinates().getZ()+forceVector[2]);

            }
        }

        // Zuruecksetzen der berechneten Koordinaten auf den Ursprung (einfach, weil es huebscher waere)
        double[] reset = resetPosition(graph);
        for (Node node:nodes) {
            node.getCoordinates().setX(round(node.getCoordinates().getX()+reset[0],5));
            node.getCoordinates().setY(round(node.getCoordinates().getY()+reset[1],5));
            node.getCoordinates().setZ(round(node.getCoordinates().getZ()+reset[2],5));
        }
    }

    public static double[] forceRepelVector (Node node, HashSet<Node> nodes, Graph graph) {
        double[] forceVector={0,0,0};

        double force;

        //Abstoßend
        for (Node itnode:nodes) {
            if (itnode == node) {
                continue;
            } else {
                force=repForce(node.getCoordinates(), itnode.getCoordinates());

                //Aufaddieren und normieren
                forceVector[0]+=(((itnode.getCoordinates().getX()-node.getCoordinates().getX())*force))/node.getCoordinates().calcDistance(itnode.getCoordinates());
                forceVector[1]+=(((itnode.getCoordinates().getY()-node.getCoordinates().getY())*force))/node.getCoordinates().calcDistance(itnode.getCoordinates());
                forceVector[2]+=(((itnode.getCoordinates().getZ()-node.getCoordinates().getZ())*force))/node.getCoordinates().calcDistance(itnode.getCoordinates());

            }
        }
        return forceVector;
    }

    public static double[] forceAttVector (Node node, HashSet<Node> partner, Graph graph) {
        double[] posChange=new double[3];

        //Anziehend
        for (Node edgePartner:partner) {
            if (node.getCoordinates().calcDistance(edgePartner.getCoordinates())>getNodePartnerEdge(node, edgePartner, graph).getLength()) {
                double shorten=1-1/node.getCoordinates().calcDistance(edgePartner.getCoordinates())*getNodePartnerEdge(node, edgePartner, graph).getLength();
                posChange[0]+=(edgePartner.getCoordinates().getX()-node.getCoordinates().getX())/node.getCoordinates().calcDistance(edgePartner.getCoordinates())*shorten;
                posChange[1]+=(edgePartner.getCoordinates().getY()-node.getCoordinates().getY())/node.getCoordinates().calcDistance(edgePartner.getCoordinates())*shorten;
                posChange[2]+=(edgePartner.getCoordinates().getZ()-node.getCoordinates().getZ())/node.getCoordinates().calcDistance(edgePartner.getCoordinates())*shorten;
            }
        }
        return posChange;
    }

    public static double repForce (Koordinaten startnode, Koordinaten endnode) {
        double force=-C*Math.pow(K,2)/startnode.calcDistance(endnode);
        return force;
    }

    public static double[] resetPosition (Graph graph) {
        double[] mean=new double[3];
        HashSet<Node> nodes = graph.getSetNodes();

        for (Node node:nodes) {
            mean[0]-=node.getCoordinates().getX();
            mean[1]-=node.getCoordinates().getY();
            mean[2]-=node.getCoordinates().getZ();
        }

        mean[0]/=nodes.size();
        mean[1]/=nodes.size();
        mean[2]/=nodes.size();

        return mean;
    }

    public static Node getPartnerKoordinaten (Node node, Edge edge, Graph graph) {
        // Nicht huebsch
        Node tmp = null;
        Node out = null;
        tmp=graph.getEndNode(edge);
        if (tmp != node) {
            out=tmp;
        } else {
            tmp=graph.getStartNode(edge);
            if (tmp != node) {
                out=tmp;
            } else {
                //ERROR!!!!!
            }
        }
        return out;
    }

    public static Edge getNodePartnerEdge (Node node, Node partner, Graph graph) {
        Edge nodePartnerEdge = null;
        HashSet<Edge> edges=graph.getSetEdges(node);
        for (Edge edge:edges) {
            if (getPartnerKoordinaten(node, edge, graph) == partner) {
                nodePartnerEdge=edge;
            }
        }
        return nodePartnerEdge;
    }

    public static double round(double value, int places) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
