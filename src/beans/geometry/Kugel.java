package beans.geometry;

/**
 * Created by Mika on 09.11.2016.
 */
public class Kugel
{
    private Koordinaten midPoint;
    private double radius;

    /**
     * Defaultkonstruktor <br><br>
     *
     * midPoint = (0, 0, 0) <br>
     * radius = 0
     */
    public Kugel()
    {
        this.midPoint = new Koordinaten();
        this.radius = 0;
    }

    /**
     * Konstruktor
     *
     * @param radius
     * @param midPoint
     */
    public Kugel(double radius, Koordinaten midPoint)
    {
        this.radius = radius;
        this.midPoint = midPoint;
    }


    public void setMidPoint(Koordinaten mid)
    {
        this.midPoint = mid;
    }

    public void setRadius(double radius)
    {
        this.radius = radius;
    }

    public Koordinaten getMidPoint()
    {
        return this.midPoint;
    }

    public double getRadius()
    {
        return this.radius;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj)
    {
        if(obj instanceof Kugel)
        {
            Kugel sphere = (Kugel) obj;

            if(this.midPoint.equals(sphere.midPoint))
            {
                return this.radius == sphere.getRadius();
            }
            return false;
        }
        return false;
    }

    public String toString()
    {
        return "Mittelpunkt: " + this.midPoint + " Radius: " + getRadius();
    }
}
