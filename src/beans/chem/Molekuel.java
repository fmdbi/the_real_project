package beans.chem;


import org.apache.log4j.Logger;
import datatype.BidiMapAtom;

import java.util.HashSet;
import java.util.Map;

public class Molekuel 
{
	private static final Logger logger = Logger.getLogger(Molekuel.class);
	
	private BidiMapAtom mapBindungen;
	private HashSet<Atom> setAtome;
	
	/**
	 * Defaultkonstruktor
	 */
	public Molekuel()
	{
		this.mapBindungen = new BidiMapAtom();
		this.setAtome = new HashSet<Atom>();
	}
	
	/**
	 * Konstruktor mit allen Informationen
	 * @param map
	 */
	public Molekuel(BidiMapAtom map)
	{
		this.mapBindungen = map;
		this.setAtome = new HashSet<Atom>();

		for(Map.Entry<MultiKeyAtom, Bindung> entry : map.getEntrySet())
		{
			Atom first = entry.getKey().getFirstKey();
			Atom second = entry.getKey().getSecondKey();

			this.setAtome.add(first);
			this.setAtome.add(second);
		}
	}
	
	/*-----------------------------------------------
	 *					Methoden
	 ----------------------------------------------*/
	
	/**
	 * Fuegt der Bindungsmap eine zusaetzliche Bindung hinzu
	 * 
	 * @param vonA
	 * @param bisA
	 * @param bindung
	 */
	public void addBindung(Atom vonA, Atom bisA, Bindung bindung)
	{
		this.mapBindungen.putEntry(new MultiKeyAtom(vonA, bisA), bindung);
		this.setAtome.add(vonA);
		this.setAtome.add(bisA);
	}
	
	/*-----------------------------------------------
	 *				Getter / Setter
	 ----------------------------------------------*/
	
	/**
	 * Getter Startatom bei zugehoeriger Bindung
	 * 
	 * @param bindung
	 * @return
	 */
	public Atom getStartatomByBindung(Bindung bindung)
	{
		MultiKeyAtom kAtoms = this.mapBindungen.getKeyByValue(bindung);
		
		if(bindung.getRichtung() < 0)
		{
			return kAtoms.getSecondKey();
		}
		else
		{
			return kAtoms.getFirstKey();
		}
	}
	
	/**
	 * Getter Zielatom bei zugehoeriger Bindung
	 * 
	 * @param bindung
	 * @return
	 */
	public Atom getZielatomByBindung(Bindung bindung)
	{
		MultiKeyAtom kAtoms = this.mapBindungen.getKeyByValue(bindung);
		
		if(bindung.getRichtung() > 0)
		{
			return kAtoms.getSecondKey();
		}
		else
		{
			return kAtoms.getFirstKey();
		}
	}

	/**
	 * Getter Liste aller Atome des Molekuels
	 *
	 * @return listAtome Atomliste
	 */
	public HashSet<Atom> getSetAtome()
	{
		return this.setAtome;
	}

	/**
	 * Getter Map-Bindungen
	 * 
	 * @return
	 */
	public BidiMapAtom getMapBindungen()
	{
		return this.mapBindungen;
	}
	
	/**
	 * Setter Map-Bindungen
	 * 
	 * @param mapBindungen
	 */
	public void setMapBindungen(BidiMapAtom mapBindungen)
	{
		this.mapBindungen = mapBindungen;
	}
}
