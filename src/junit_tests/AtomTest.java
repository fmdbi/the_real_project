package junit_tests;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import beans.chem.Atom;
import beans.geometry.Koordinaten;

public class AtomTest
{
	private static final Logger logger = Logger.getLogger(AtomTest.class);

	@Before
	public void setUp() throws Exception
	{
	}

	@Test
	public void testAtomID() 
	{
		Atom atom = new Atom(1, "H", new Koordinaten(1,1,1));
		assertEquals(1, atom.getIdAtom());
	}
	
	@Test
	public void testAtomElement()
	{
		Atom atom = new Atom(1, "H", new Koordinaten(1, 1, 1));
		assertEquals("H", atom.getElement());
	}
}
