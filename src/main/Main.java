package main;

import beans.chem.Molekuel;
import beans.graph.Component;
import beans.graph.Edge;
import beans.graph.Graph;
import beans.graph.Node;
import logic.ChemicalFileFormat;
import logic.CombinatorialEmbedding;
import logic.ForceFieldEmbedding;
import logic.PebbleGame;
import org.apache.log4j.Logger;

import java.util.HashSet;

public class Main
{
    private static final Logger logger = Logger.getLogger(Main.class);

    private static final String PROGRAM_NAME = "molDistEmbed";
    private static final int REQUIRED_ARGS = 4;

    private static final int MOLECULAR_CONJECTURE_EDGE_FACTOR = 5;
    private static final int PEBBLE_GAME_K = 6;
    private static final int PEBBLE_GAME_L = 6;
    private static final double EPSILON_EDGE_LENGTH_DIFF = 0.00009; // Nur für Anzeige!

    public static void main(String[] args)
    {
        boolean allOptionsSet = true;
        double distancesToBeRemoved = 0;
        int chosenEmbedder = -1;
        int rigidityCode;
        Molekuel inputMolecule = new Molekuel();
        String outFile = "";

        logger.info("\n" + PROGRAM_NAME + "\n");
        logger.info("Tries to embed a molecule graph representation only determined by pairwise distances in 3D.");
        logger.info("\nAuthors: Alexander Engler, Christian Heide, Markus Michaelis, Sophie Wolf\n");

        if (args.length != REQUIRED_ARGS * 2)
        {
            Main.printUsageError();
        }
        else
        {
            boolean cancelLoop = false;
            for (int i = 0; i < args.length - 1; i += 2)
            {
                if (cancelLoop)
                {
                    break;
                }
                switch (args[i])
                {
                    case "-i":
                        logger.info("Read SDF file from " + args[i + 1]);
                        inputMolecule = new Molekuel(ChemicalFileFormat.readSDF(args[i + 1]));
                        break;
                    case "-o":
                        logger.info("Write SDF file to " + args[i + 1]);
                        outFile = args[i + 1];
                        break;
                    case "-r":
                        logger.info("Remove " + (Double.parseDouble(args[i + 1]) * 100) + "% randomly drawn distances from input molecule to simulate real-data input");
                        distancesToBeRemoved = Double.parseDouble(args[i + 1]);
                        break;
                    case "-e":
                        if (args[i + 1].equals("ff"))
                        {
                            logger.info("Using force field embedder");
                            chosenEmbedder = 0;
                        }
                        else if (args[i + 1].equals("combi"))
                        {
                            logger.info("Using combinatorial embedder");
                            chosenEmbedder = 1;
                        }
                        else
                        {
                            cancelLoop = true;
                            allOptionsSet = false;
                        }
                        break;
                    default:
                        cancelLoop = true;
                        allOptionsSet = false;
                        break;
                }
            }

            if (allOptionsSet)
            {
                Graph completeGraph = new Graph(inputMolecule);

                int numberOfEdges = completeGraph.getSetEdges().size();

                completeGraph.randomlyRemoveEdges(distancesToBeRemoved);

                logger.info("Complete graph (SDF imput) has " + numberOfEdges + " edges.");
                logger.info("Remove " + (numberOfEdges - completeGraph.getSetEdges().size()) + " edges to simulate real-data input.");
                logger.info("Graph G of simulated input data has " + completeGraph.getSetEdges().size() + " edges left.");

                PebbleGame pgCompleteGraph = new PebbleGame(completeGraph, PEBBLE_GAME_K, PEBBLE_GAME_L);
                completeGraph.modelMolecularConjecture(MOLECULAR_CONJECTURE_EDGE_FACTOR);

                rigidityCode = pgCompleteGraph.testRigidity(); // Play pebble game

                logger.info("Nodes: " + completeGraph.getSetNodes());
                logger.info("|Nodes|: " + completeGraph.getSetNodes().size());
                logger.info("|Pebbles left|: " + pgCompleteGraph.countFreePebbles());
                logger.info("|Edges|: " + completeGraph.getSetEdges().size());
                logger.info("|Redundant Edges|: " + completeGraph.getSetRedundantEdges().size());

                completeGraph.removeMolecularConjectureEdges();

                switch (rigidityCode)
                {
                    case 0:
                        logger.info("Source graph G is well-constrained");
                        break;
                    case 1:
                        logger.info("Source graph G is flexible");
                        break;
                    case 2:
                        logger.info("Source graph G is redundantly rigid");
                        break;
                    case 3:
                        logger.info("Source graph G is 'other'");
                        break;
                }

                HashSet<Component> rigidComponents = pgCompleteGraph.getRigidComponents();

                logger.info("\n### Components ###");
                for (Component component : rigidComponents)
                {
                    Graph componentSubgraph = completeGraph;
                    /* Wenn die Component den kompletten Graphen umfasst, können wir uns die
                    erneute Prüfung sparen: */
                    if (component.getNodes().size() != completeGraph.getSetNodes().size())
                    {
                        componentSubgraph = component.toGraph(completeGraph);
                        PebbleGame pgOnComponentSubgraph = new PebbleGame(componentSubgraph, PEBBLE_GAME_K, PEBBLE_GAME_L);
                        componentSubgraph.modelMolecularConjecture(MOLECULAR_CONJECTURE_EDGE_FACTOR);

                        rigidityCode = pgOnComponentSubgraph.testRigidity(); // Play pebble game

                        logger.info("Nodes: " + component.getNodes());
                        logger.info("|Nodes|: " + componentSubgraph.getSetNodes().size());
                        logger.info("|Pebbles left|: " + pgOnComponentSubgraph.countFreePebbles());
                        logger.info("|Edges|: " + componentSubgraph.getSetEdges().size());
                        logger.info("|Redundant Edges|: " + componentSubgraph.getSetRedundantEdges().size());

                        componentSubgraph.removeMolecularConjectureEdges();

                        switch (rigidityCode)
                        {
                            case 0:
                                logger.error("Component is well-constrained");
                                break;
                            case 1:
                                logger.error("Component is flexible");
                                break;
                            case 2:
                                logger.info("Component is redundantly rigid");
                                break;
                            case 3:
                                logger.error("Component is 'other'");
                                break;
                        }
                    }
                    if (rigidityCode == 2) /* Required constraint met */
                    {
                        for (Node node : componentSubgraph.getSetNodes())
                        {
                            if (componentSubgraph.getSetEdges(node).size() < 4)
                            {
                                logger.error("Not 4-connected! Embedding not unambiguous. " + componentSubgraph.getSetEdges(node).size());
                            }
                        }

                        boolean embeddingHasWorked = false;

                        if (chosenEmbedder == 0)
                        {
                            ForceFieldEmbedding.ForceByLength(componentSubgraph, 1);
                            embeddingHasWorked = true;
                            ChemicalFileFormat.writeSDF(componentSubgraph, componentSubgraph.getIdGraph() + outFile, false);
                        }
                        else if (chosenEmbedder == 1)
                        {
                            if (CombinatorialEmbedding.embedGraph(componentSubgraph))
                            {
                                embeddingHasWorked = true;
                                ChemicalFileFormat.writeSDF(componentSubgraph, componentSubgraph.getIdGraph() + outFile, false);
                            }
                        }

                        if (embeddingHasWorked)
                        {
                            logger.info("\nEdge length constraints \tEmbedded edge lengths \t\tRating (diff < " + EPSILON_EDGE_LENGTH_DIFF + ")");
                            for (Edge edge : componentSubgraph.getSetEdges())
                            {
                                double embeddedDistance = componentSubgraph.getStartNode(edge).getCoordinates().calcDistance(componentSubgraph.getEndNode(edge).getCoordinates());
                                String state;
                                if (Math.abs(edge.getLength() - embeddedDistance) < EPSILON_EDGE_LENGTH_DIFF) // Nur für Anzeige, nicht wichtig!
                                {
                                    state = "OK";
                                }
                                else
                                {
                                    state = "Error";
                                }
                                logger.info(edge.getLength() + "\t\t\t" + embeddedDistance + "\t\t\t" + state);
                            }

                            logger.info("\nNode \tX-coordinate \t\tY-coordinate \t\tZ-coordinate");
                            for (Node loopNode : componentSubgraph.getSetNodes())
                            {
                                logger.info(loopNode + "\t" + loopNode.getCoordinates());
                            }
                        }
                        else
                        {
                            logger.error("Can't embed graph.");
                        }
                    }
                }
            }
            else
            {
                Main.printUsageError();
            }
        }
    }

    private static void printUsageError()
    {
        logger.error("Missing required argument(s).");
        logger.error("Usage: -i <String> -o <String> -r <Float> -e <String>");
        logger.error("-i: \tFilename of the input sdf file (should contain complete molecule files. Used to simulate real-data input)");
        logger.error("-o: \tSuffix of the output sdf file (subgraph-id will be attached to the front)");
        logger.error("-r: \tHow many distances (percent; 0.0 - 1.0) should be removed from input molecule to simulate real-data input?");
        logger.error("-e: \tWhich graph embedder should be used (\"ff\" or \"combi\")?");
        logger.error("Example: java -jar " + PROGRAM_NAME + ".jar -i caffeine.sdf -o caffeine_dist_combi.sdf -r 0.5 -e combi");
    }

}
