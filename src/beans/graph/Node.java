package beans.graph;

import beans.geometry.Koordinaten;
import org.apache.log4j.Logger;

/**
 * Node-Klasse
 *
 * @author Mika
 */
public class Node extends GraphElement
{
    private static final Logger logger = Logger.getLogger(Node.class);
    public static int count = 0;

    private int idNode;
    private String label = null;
    private int countPebbles = 0;
    private Koordinaten coordinates = null;

    /**
     * Default-Konstruktor
     */
    public Node()
    {
        this.idNode = count++;
        this.coordinates = new Koordinaten();
    }

    /**
     * Konstruktor fuer Node ohne Pebbles
     *
     * @param id
     */
    public Node(int id)
    {
        count++;
        this.idNode = id;
        this.coordinates = new Koordinaten();
    }

    /**
     * Pebbles um eins erhöhen
     */
    public void incrPebbles()
    {
        this.countPebbles++;
    }

    /**
     * Pebbles um eins reduzieren
     *
     * @throws Exception
     */
    void decrPebbles() throws Exception
    {
        if (this.countPebbles > 0)
        {
            this.countPebbles--;
        }
        else
        {
            throw new Exception("No pebble available");
        }
    }

    /**
     * Covert Edge unter Berücksichtigung von Pebbles
     * Gibt bei Gelingen true zurück.
     *
     * High-Level
     *
     * @param graph zugehoeriger Graph
     * @param edge edge Edge, die von diesem Node gecovert werden soll
     * @return True, wenn es funktioniert hat, sonst false
     */
    public boolean coverEdge(Graph graph, Edge edge)
    {
        try
        {
            edge.orientAway(graph, this);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString()
    {
        return "" + this.idNode;
    }

	/*--------------------------------------------
     *			Setter- und Gettermethoden
	 *------------------------------------------*/

    /**
     * Getter Anzahl Pebbles
     *
     * @return Anzahl der Pebbles dieses Nodes
     */
    public int getCountPebbles()
    {
        return this.countPebbles;
    }

    /**
     * Setter Anzahl Pebbles
     *
     * @param countPebbles
     */
    public void setCountPebbles(int countPebbles)
    {
        this.countPebbles = countPebbles;
    }

    /**
     * Getter Knoten-ID
     *
     * @return idNode Der Name dieses Nodes
     */
    public int getIdNode()
    {
        return this.idNode;
    }

    public void addComponentTag(Component component)
    {
        this.componentTag = component;
    }

    public Koordinaten getCoordinates()
    {
        return this.coordinates;
    }

    public void setCoordinates(Koordinaten coords)
    {
        this.coordinates = coords;
    }

    public String getLabel()
    {
        return this.label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }
}
