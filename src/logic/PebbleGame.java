package logic;

import beans.graph.*;
import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.LinkedList;

public class PebbleGame
{
    private static final Logger logger = Logger.getLogger(PebbleGame.class);

    private Graph graph;
    private int k;
    private int l;
    private HashSet<Edge> singletonPseudoEdges = new HashSet<Edge>();
    private HashSet<Component> componentMaintenanceSet = new HashSet<>();

    public PebbleGame(Graph graph, int k, int l)
    {
        this.graph = graph;
        this.k = k;
        this.l = l;
        this.setCountPebbles(k, graph);
        for (int i = 0; i < this.l; i++)
        {
            Edge mEdge = new Edge(true, false);
            this.singletonPseudoEdges.add(mEdge);
        }
    }

    /**
     * Initialisiert alle Nodes mit Pebbles
     */
    private void setCountPebbles(int countPebbles, Graph graph)
    {
        for (Node node : graph.getSetNodes())
        {
            node.setCountPebbles(countPebbles);
        }
    }

    /**
     * Vervielfacht eine gegebene Kante
     *
     * @param sourceEdge Edge, die vervielfacht werden soll
     * @return Liste von l+1 Edges
     * (l kopierte Pseudo-Edges + eine Eingabe-Edge)
     */
    private HashSet<Edge> multiplyEdge(Edge sourceEdge)
    {
        HashSet<Edge> out = new HashSet<>();

        out.add(sourceEdge);

        for (Edge mEdge : this.singletonPseudoEdges)
        {
            mEdge.setCovered(false);
            mEdge.setRedundant(false);
            this.graph.addEdge(mEdge, this.graph.getStartNode(sourceEdge), this.graph.getEndNode(sourceEdge));
            out.add(mEdge);
        }
        return out;
    }

    /**
     * Entfernt Pseudo-Edges
     *
     * @param mEdges Menge von Edges, aus der Pseudo-Edges entfernt werden sollen.
     *               Vermeidet das Iterieren über alle Edges.
     */
    private void removePseudoEdges(HashSet<Edge> mEdges)
    {
        HashSet<Edge> toBeDeleted = new HashSet<>();

        for (Edge edge : mEdges)
        {
            if (edge.isPseudo())
            {
                toBeDeleted.add(edge);
            }
        }
        for (Edge edgeToBeDeleted : toBeDeleted)
        {
            if (edgeToBeDeleted.isCovered())
            {
                this.graph.getStartNode(edgeToBeDeleted).incrPebbles();
            }
            this.graph.deleteEdge(edgeToBeDeleted);
        }
    }

    /**
     * Spielt das Pebble-Game.
     * Entfernt redundante Edges und detektiert Components.
     */
    private void coverAllEdges() // TODO: CD kann beschleunigt werden (vgl. Lee & Streinu, 2008)
    {
        LinkedList<GraphElement> pebblePath;

        for (Edge existingEdge : this.graph.getSetNonPseudoEdges())
        {
            if (!existingEdge.isRedundant() && !existingEdge.isCovered())
            {
                HashSet<Edge> mEdges = this.multiplyEdge(existingEdge);
                for (Edge mEdge : mEdges) // Mit vervielfachter Edge spielen
                {
                    if (this.graph.getStartNode(mEdge).coverEdge(this.graph, mEdge))
                    {
                        //logger.info("Node " + this.graph.getStartNode(mEdge).getIdNode() + " covert Edge " + mEdge.getIdEdge() + ".");
                    }
                    else if (this.graph.getEndNode(mEdge).coverEdge(this.graph, mEdge))
                    {
                        //logger.info("Node " + this.graph.getStartNode(mEdge).getIdNode() + " covert Edge " + mEdge.getIdEdge() + ".");
                    }
                    else
                    {
                        //logger.info("Edge " + mEdge.getIdEdge() + " kann nicht trivial gecovert werden - suche verfügbare Pebbles entlang des Graphen.");

                        pebblePath = new LinkedList<>();

                        if (this.findFreePebbles(this.graph.getStartNode(mEdge), pebblePath, new LinkedList<>()))
                        {
                            this.movePebbleAlongPath(pebblePath);
                            if (this.graph.getStartNode(mEdge).coverEdge(this.graph, mEdge))
                            {
                                //logger.info("Node " + this.graph.getStartNode(mEdge).getIdNode() + " covert Edge " + mEdge.getIdEdge() + " (nicht-trivial).");
                            }
                            else
                            {
                                logger.error("Dies ist keine Übung! Bitte bleiben Sie zuhause und halten Sie Fenster und Türen geschlossen.");
                            }
                        }
                        else
                        {
                            if (this.findFreePebbles(this.graph.getEndNode(mEdge), pebblePath, new LinkedList<>()))
                            {
                                this.movePebbleAlongPath(pebblePath);
                                if (this.graph.getEndNode(mEdge).coverEdge(this.graph, mEdge))
                                {
                                    //logger.info("Node " + this.graph.getStartNode(mEdge).getIdNode() + " covert Edge " + mEdge.getIdEdge() + " (nicht-trivial).");
                                }
                                else
                                {
                                    logger.error("Dies ist keine Übung! Bitte bleiben Sie zuhause und halten Sie Fenster und Türen geschlossen.");
                                }
                            }
                            else
                            {
                                if (existingEdge.isCovered() && !existingEdge.isRedundant())
                                {
                                    this.graph.getStartNode(existingEdge).incrPebbles();
                                }
                                existingEdge.setRedundant(true);
                                //logger.info("Edge " + existingEdge.getIdEdge() + " kann, mangels erreichbarem Pebble, nicht gecovert werden - sie ist redundant.");
                                break; // Nach erfolgter Redundanzerkennung kann an dieser Stelle abgebrochen werden
                            }
                        }
                    }
                }
                this.maintainComponents(this.detectComponent(existingEdge));
                this.removePseudoEdges(mEdges);
            }
        }
    }

    /**
     * Findet freie Pebbles entlang des gerichteten Graphen.
     * Gibt true zurück, wenn Pebble gefunden wurde.
     * Der Pfad befindet sich dann in LinkedList<GraphElement> queue.
     *
     * @param node    Start-Node für die Suche
     * @param queue   Anfangs leere Liste, in der die rekursive Funktion den Pfad zu einem freien Pebble speichert
     * @param visited Anfangs leere Liste, in der sich die rekursive Funktion schon besuchte Knoten merkt
     */
    private boolean dfs(Node node, LinkedList<GraphElement> queue, LinkedList<Node> visited)
    {
        if (visited.contains(node))
        {
            return false;
        }

        if (node.getCountPebbles() > 0)
        {
            visited.add(node);
            return true;
        }

        visited.add(node);

        HashSet<Node> alreadySeenNodes = new HashSet<>();
        for (Edge edge : this.graph.getSetEdges(node))
        {
            if (edge.isCoveredBy(this.graph, node) && !edge.isRedundant()) // Zweite Bedingung erhöht Geschwindigkeit (?)
            {
                Node endNode = graph.getEndNode(edge);
                if (!alreadySeenNodes.contains(endNode))
                {
                    alreadySeenNodes.add(endNode);
                    if (this.dfs(this.graph.getEndNode(edge), queue, visited))
                    {
                        queue.addLast(this.graph.getEndNode(edge));
                        queue.addLast(edge);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean findFreePebbles(Node node, LinkedList<GraphElement> queue, LinkedList<Node> visited)
    {
        return this.dfs(node, queue, visited);
    }

    private boolean reach(Node node, LinkedList<GraphElement> queue, LinkedList<Node> visited)
    {
        return this.dfs(node, queue, visited);
    }

    /**
     * Transferiert Pebble über gegebenen Pfad
     *
     * @param pebblePath Pfad aus findFreePebbles(), der traversiert werden soll
     */
    private void movePebbleAlongPath(LinkedList<GraphElement> pebblePath)
    {
        Node pebbleSourceNode = (Node) pebblePath.getFirst();

        //logger.info("Hole Pebble von Node " + pebbleSourceNode.getIdNode() + ".");

        while (pebblePath.size() > 0)
        {
            GraphElement element = pebblePath.pop();

            GraphElement nextElement;
            Node node;
            Edge edge;

            if (element.getClass() == Node.class)
            {
                node = (Node) element;
                nextElement = pebblePath.pop();
                if (nextElement.getClass() == Edge.class)
                {
                    edge = (Edge) nextElement;
                    node.coverEdge(this.graph, edge);
                }
                else
                {
                    logger.error("Expected edge on queue, found node.");
                }
            }
            else if (element.getClass() == Edge.class)
            {
                logger.error("Expected node on queue, found edge.");
            }
        }
    }

    /**
     * Bestimmt Gesamtanzahl der freien Pebbles im Graphen
     *
     * @return Anzahl aller freier Pebbles im Graphen
     */
    public int countFreePebbles()
    {
        int out = 0;

        for (Node node : this.graph.getSetNodes())
        {
            out += node.getCountPebbles();
        }
        return out;
    }

    /**
     * Prüft die Rigidity eines Graphen
     *
     * @return 0: well-constrained
     * 1: under-constrained
     * 2: over-constrained
     * 3: other
     */
    public int testRigidity()
    {
        boolean anyEdgeRejected = false;

        this.coverAllEdges();

        if (!this.graph.getSetRedundantEdges().isEmpty())
        {
            anyEdgeRejected = true;
        }

        if (this.countFreePebbles() == this.l)
        {
            if (anyEdgeRejected)
            {
                return 2; // over-constrained
            }
            else
            {
                return 0; // well-constrained
            }
        }
        else
        {
            if (anyEdgeRejected)
            {
                return 3; // other
            }
            else
            {
                return 1; // under-constrained
            }
        }
    }

    /**
     * Gibt detektierte rigid Components zurück
     *
     * @return
     */
    public HashSet<Component> getRigidComponents()
    {
        return this.componentMaintenanceSet;
    }

    /**
     * Detektiert die Components, die durch das Covern einer Edge
     * etabliert werden.
     *
     * @return Menge von Nodes, die zur detektierten Component gehören
     */
    private HashSet<Node> detectComponent(Edge edge)
    {
        Node u = this.graph.getStartNode(edge);
        Node v = this.graph.getEndNode(edge);
        Node w;
        LinkedList<Node> reachU = new LinkedList<>();
        LinkedList<Node> reachV = new LinkedList<>();
        LinkedList<Node> reachW;
        HashSet<Node> vPrime = new HashSet<>();
        HashSet<Edge> setEdges = this.graph.getSetNonPseudoEdges();
        HashSet<Node> queueTmp;
        LinkedList<Node> queue = new LinkedList<>();

        if ((u.getCountPebbles() + v.getCountPebbles()) > 0) // Edge is free
        {
            return null;
        }
        if (this.reach(u, new LinkedList<>(), reachU))
        {
            return null;
        }
        if (this.reach(v, new LinkedList<>(), reachV))
        {
            return null;
        }
        vPrime.addAll(reachU);
        vPrime.addAll(reachV);
        queueTmp = new HashSet<>(); // Hilfsstruktur; nur Nodes, die in vPrime zeigen, werden dann queue hinzugefügt
        queueTmp.addAll(this.graph.getSetNodes());
        queueTmp.removeAll(vPrime);

        for (Node queueNode : queueTmp)
        {
            for (Edge queueEdge : this.graph.getSetEdges(queueNode))
            {
                if (!queueEdge.isPseudo() && queueEdge.isCoveredBy(this.graph, queueNode) && vPrime.contains(this.graph.getEndNode(queueEdge)) && !queue.contains(queueNode))
                {
                    queue.add(queueNode);
                }
            }
        }
        //logger.info("Q: " + queue);

        queueTmp = new HashSet<>(); // Hilfsstruktur, speichert alle schon enqueueten Nodes
        queueTmp.addAll(queue);

        while (queue.size() > 0)
        {
            w = queue.pop();
            reachW = new LinkedList<>();
            if (!reach(w, new LinkedList<>(), reachW))
            {
                vPrime.addAll(reachW);
            }

            for (Edge reachWEdge : setEdges)
            {
                if (reachWEdge.isCovered() && reachW.contains(this.graph.getEndNode(reachWEdge)))
                {
                    if (!queueTmp.contains(this.graph.getStartNode(reachWEdge)))
                    {
                        queueTmp.add(this.graph.getStartNode(reachWEdge));
                        queue.add(this.graph.getStartNode(reachWEdge));
                    }
                }
            }

        }
        //logger.info("End-V': " + vPrime + "\n");
        return vPrime;
    }

    /**
     * Ordnet Nodes Components zu
     *
     * @param nodesFormingComponent Liste von Nodes aus Graph.detectComponents()
     */
    private void maintainComponents(HashSet<Node> nodesFormingComponent)
    {

        if (nodesFormingComponent != null) // sonst (== null): free edge
        {
            Component newComponent = new Component();
            this.componentMaintenanceSet.add(newComponent);

            for (Node node : nodesFormingComponent)
            {
                node.addComponentTag(newComponent);
                newComponent.addNode(node);
            }

            HashSet<Component> copyComponents = new HashSet<>();
            copyComponents.addAll(this.componentMaintenanceSet);
            for (Component component : copyComponents)
            {
                if (component != newComponent)
                {
                    boolean toBeRemoved = true;
                    for (Node node : component.getNodes())
                    {
                        if (node.getComponentTag() != newComponent)
                        {
                            toBeRemoved = false;
                            break;
                        }
                    }
                    if (toBeRemoved)
                    {
                        this.componentMaintenanceSet.remove(component);
                    }
                }
            }

        }
    }

}
