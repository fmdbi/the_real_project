package datatype;

import beans.chem.Bindung;
import beans.chem.MultiKeyAtom;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class BidiMapAtom
{
	private static final Logger logger = Logger.getLogger(BidiMapAtom.class);

	private HashMap<MultiKeyAtom, Bindung> mapAtomBindung = new HashMap<>();
	private HashMap<Bindung, MultiKeyAtom> mapBindungAtom = new HashMap<>();
	
	public BidiMapAtom()
	{
		this.mapAtomBindung = new HashMap<MultiKeyAtom, Bindung>();
		this.mapBindungAtom = new HashMap<Bindung, MultiKeyAtom>();
	}
	
	public BidiMapAtom(MultiKeyAtom key, Bindung value)
	{
		mapAtomBindung.put(key, value);
		mapBindungAtom.put(value, key);
	}
	
	public void putEntry(MultiKeyAtom key, Bindung value)
	{
		this.mapAtomBindung.put(key, value);
		this.mapBindungAtom.put(value, key);
	}
	
	public Bindung getValueByKey(MultiKeyAtom key)
	{
		return this.mapAtomBindung.get(key);
	}
	
	public MultiKeyAtom getKeyByValue(Bindung value)
	{
		return this.mapBindungAtom.get(value);
	}
	
	public HashMap<MultiKeyAtom, Bindung> getMapAtomBindung()
	{
		return this.mapAtomBindung;
	}
	
	public HashMap<Bindung, MultiKeyAtom> getMapBindungAtom()
	{
		return this.mapBindungAtom;
	}

	public Set<Map.Entry<MultiKeyAtom, Bindung>> getEntrySet()
	{
		return this.mapAtomBindung.entrySet();
	}
}
