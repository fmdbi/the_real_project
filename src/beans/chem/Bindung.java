package beans.chem;

import org.apache.log4j.Logger;

public class Bindung
{
	private static final Logger logger = Logger.getLogger(Bindung.class);
	private int richtung;
	private int bTyp;

    /**
	 * Defaultkonstruktor<br> 
	 * <br>
	 * <b>Richtung</b> = 1 --> von A nach B <br>
	 * <b>BTyp</b> = 1 --> Single<br>
	 * <b>IsPseudo</b> = false --> kein Pseudoknoten
	 */
	public Bindung()
	{
		this.richtung = 1;
		this.bTyp = 1;
    }
	
	/**
	 * Konstruktor mit allen Informationen<br> 
	 * <br>
	 * <b>richtung</b> >= 0 --> von A nach B.<br>
	 * <b>richtung</b> < 0 --> von B nach A.<br>
	 * <br>
	 * <b>typ</b>:<br>
	 * 1 --> Single<br>
	 * 2 --> Double<br>
	 * 3 --> Triple<br>
	 * 4 --> Aromatic<br>
	 * 5 --> Single or Double<br>
	 * 6 --> Single or Aromatic<br>
	 * 7 --> Double or Aromatic<br>
	 * 8 --> Any<br>
	 * <br>
	 * <b>pseudo</b> = true --> ist Pseudoknoten<br>
	 * <b>pseudo</b> = false --> ist kein Pseudoknoten
	 * 
	 * @param richtung
	 * @param typ
	 * @param pseudo
	 */
	public Bindung(int richtung, int typ, boolean pseudo)
	{
		this.richtung = richtung;
		this.bTyp = typ;
    }
	
	/*-----------------------------------------------
	 *					Methoden
	 ----------------------------------------------*/
	
	/**
	 * Dreht die Kantenrichtung um
	 */
	public void switchBindung()
	{
		this.richtung = this.richtung * -1;
	}
	
	/*-----------------------------------------------
	 *				Getter / Setter
	 ----------------------------------------------*/
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof Bindung))
        {
            return false;
        }
        else
        {
            return this.richtung == (((Bindung) obj).getRichtung());
        }
    }

    /**
     * Getter Richtung
     *
     * @return
	 */
	public int getRichtung()
	{
		return this.richtung;
	}
	
	/**
	 * Getter Bindungstyp
     *
     * @return
	 */
	public int getBTyp()
	{
		return this.bTyp;
	}

	/**
	 * Setter Bindungstyp<br>
	 * <br>
	 * <b>bTyp</b>:<br>
	 * 1 --> Single<br>
	 * 2 --> Double<br>
	 * 3 --> Triple<br>
	 * 4 --> Aromatic<br>
	 * 5 --> Single or Double<br>
	 * 6 --> Single or Aromatic<br>
	 * 7 --> Double or Aromatic<br>
	 * 8 --> Any<br>
     *
     * @param bTyp
	 */
	public void setBTyp(int bTyp)
	{
		this.bTyp = bTyp;
	}

	/**
     * Setter Richtung<br>
     * <br>
     * <b>richtung</b> >= 0 --> von A nach B.<br>
     * <b>richtung</b> < 0 --> von B nach A.<br>
     *
     * @param richtung
     */
    public void setRichtung(int richtung)
    {
        this.richtung = richtung;
    }

    public String toString()
	{
        return "Richtung: " + this.richtung + " Bindungstyp: " + this.bTyp;
    }
}
