package beans.graph;

import org.apache.log4j.Logger;

import java.util.HashSet;

public class Component
{
    private static final Logger logger = Logger.getLogger(Component.class);
    private static int maxComponent = 0;
    private int id;
    private HashSet<Node> nodes = new HashSet<>();

    public Component()
    {
        this.id = Component.maxComponent++;
    }

    /**
     * Node der Component hinzufügen
     *
     * @param node
     */
    public void addNode(Node node)
    {
        this.nodes.add(node);
    }

    /**
     * Nodes aus Component entfernen
     * @param node
     */
    public void deleteNode(Node node)
    {
        this.nodes.remove(node);
    }

    /**
     * Erzeugt einen Graphen aus der Component
     *
     * @param sourceGraph Der Quell-Graph, auf dem das Pebble Game ausgeführt wurde
     * @return (Sub-)Graph
     */
    public Graph toGraph(Graph sourceGraph)
    {
        Graph graph = new Graph(this.id + "");
        HashSet<Node> nodesOfComponent = this.getNodes();
        for (Node node : nodesOfComponent)
        {
            for (Edge edge : sourceGraph.getSetEdges(node))
            {
                Node startNode = sourceGraph.getStartNode(edge);
                Node endNode = sourceGraph.getEndNode(edge);
                if (nodesOfComponent.contains(startNode))
                {
                    if (nodesOfComponent.contains(endNode))
                    {
                        if (!graph.getSetEdges().contains(edge))
                        {
                            edge.setRedundant(false);
                            edge.setCovered(false);
                            graph.addEdge(edge, startNode, endNode);
                        }
                    }
                }
            }
        }
        return graph;
    }

    /*--------------------------------------------
    *			Setter- und Gettermethoden
    *-------------------------------------------*/

    public HashSet<Node> getNodes()
    {
        return this.nodes;
    }

    public int getId()
    {
        return id;
    }

    public String toString()
    {
        return id + "";
    }

}
