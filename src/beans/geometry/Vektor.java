package beans.geometry;

import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by Mika on 09.11.2016.
 */
public class Vektor
{
    private static final Logger logger = Logger.getLogger(Koordinaten.class);

    private static final int SQUARE = 2;
    private static final BigDecimal TWO = new BigDecimal(2);

    private double x;
    private double y;
    private double z;

    public Vektor()
    {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    public Vektor(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /*
     *
     * 			Methoden
     *
     */
    private static BigDecimal sqrt(BigDecimal A, final int SCALE)
    {
        BigDecimal x0 = new BigDecimal("0");
        BigDecimal x1 = new BigDecimal(Math.sqrt(A.doubleValue()));
        while (!x0.equals(x1))
        {
            x0 = x1;
            x1 = A.divide(x0, SCALE, RoundingMode.HALF_UP);
            x1 = x1.add(x0);
            x1 = x1.divide(TWO, SCALE, RoundingMode.HALF_UP);
        }
        return x1;
    }

    public double calcLength()
    {
        double sumSquares = Math.pow(x, SQUARE) + Math.pow(y, SQUARE) + Math.pow(z, SQUARE) ;
        return sqrt(new BigDecimal(sumSquares), 10).doubleValue();
    }

	/*
	 *
	 * 			GETTER / SETTER
	 *
	 */

    public double getX()
    {
        return this.x;
    }

    public double getY()
    {
        return this.y;
    }

    public double getZ()
    {
        return this.z;
    }

	/*
	 *
	 * 				equals() / toString()
	 *
	 */

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj)
    {
        if(obj instanceof Vektor)
        {
            if(this.x == ((Vektor) obj).getX())
            {
                if(this.y == ((Vektor) obj).getY())
                {
                    // falsche Z-Koordinate
                    return this.z == ((Vektor) obj).getZ();
                }
                // falsche Y-Koordinate
                return false;
            }
            // falsche X-Koordinate
            // return false;
        }
        // falsche Instanz
        return false;
    }

    public String toString()
    {
        return "" + this.x + ", " + this.y + ", " + this.z;
    }
}
