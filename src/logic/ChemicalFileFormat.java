package logic;

import beans.chem.Atom;
import beans.chem.Bindung;
import beans.chem.MultiKeyAtom;
import beans.geometry.Koordinaten;
import beans.graph.Edge;
import beans.graph.Graph;
import beans.graph.Node;
import datatype.BidiMapAtom;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.HashMap;
import java.util.Locale;

/**
 * Klasse ChemicalFileFormat fuer Mol-Files
 * 
 * @author Mika
 *
 */
public class ChemicalFileFormat
{
	private static final Logger logger = Logger.getLogger(ChemicalFileFormat.class);
	
	/* Row Header */
	private static final int ROW_NAME = 1;
	private static final int ROW_INFORMATION = 2;
	private static final int ROW_COMMENT = 3;
	private static final int ROW_COUNTS = 4;
	
	/* First Element */
	private static final int COL_FIRST = 0;
	
	/* Col Count */
	private static final int COL_MAX_HEAD = 12;
	private static final int COL_MAX_ATOM = 16;
	private static final int COL_MAX_BINDING = 7;
	private static final int COL_COUNT_ATOM = 0;
	private static final int COL_COUNT_BOND = 1;
	
	/* Col Atom */
	private static final int COL_ATOM_X = 0;
	private static final int COL_ATOM_Y = 1;
	private static final int COL_ATOM_Z = 2;
	private static final int COL_ATOM_NAME = 3;
	
	/* Col Bond */
	private static final int COL_BOND_FROM = 0;
	private static final int COL_BOND_TO = 1;
	private static final int COL_BOND_TYP = 2;

	/*-----------------------------------------------------------------------------------------------
	 * 
	 * 										INNERE KLASSEN
	 * 
	 -----------------------------------------------------------------------------------------------*/

	public static void writeSDF(Graph graph, String nameFile)
    {
        writeSDF(graph, nameFile, false);
    }

    public static void writeSDF(Graph graph, String nameFile, boolean writeEdges)
    {
		Writer fWriter = null;

		try
		{
			HashMap<Integer, Integer> positionNodes;
            fWriter = new FileWriter(nameFile);
            writeHeader(fWriter, graph, writeEdges);
            positionNodes = writeNodes(fWriter, graph);
            if (writeEdges)
            {
                writeEdges(fWriter, graph, positionNodes);
            }
            writeFooter(fWriter);
            fWriter.close();
		}
		catch(Exception e)
		{
			logger.error(e,e);
		}
	}

	
	
	
	/*-----------------------------------------------------------------------------------------------
     *
	 * 											METHODEN
	 * 
	 -----------------------------------------------------------------------------------------------*/

    private static void writeHeader(Writer fWriter, Graph graph, boolean writeEdges)
    {
		try
		{
			fWriter.write("ID Graph: " + graph.getIdGraph() + "\n");
			fWriter.write("\n");
			fWriter.write("\n");

			StringBuilder sBuilder = new StringBuilder();
			sBuilder.append(graph.getSetNodes().size() + "\t");
            if (writeEdges)
            {
                sBuilder.append(graph.getSetEdges().size() + "\t");
            }
            else
            {
                sBuilder.append(0 + "\t");
            }

			for(int col = 2; col < COL_MAX_HEAD; col++)
			{
				if(col == (COL_MAX_HEAD-1))
				{
					sBuilder.append("V2000\n");
				}
				else if(col == (COL_MAX_HEAD-2))
				{
					sBuilder.append("1\t");
				}
				else
				{
					sBuilder.append("" + 0 + "\t");
				}
			}

			fWriter.write(sBuilder.toString());
		}
		catch(IOException ioE)
		{
			logger.error(ioE, ioE);
		}
	}

	private static HashMap<Integer, Integer> writeNodes(Writer fWriter, Graph graph)
	{
		try
		{
			HashMap<Integer, Integer> position = new HashMap();
			StringBuilder sBuilder = new StringBuilder();
			int cRow = 0;

			for(Node node : graph.getSetNodes())
            {
                if (node.getCoordinates() != null)
                {
                    ++cRow;
                    for (int col = 0; col < COL_MAX_ATOM; col++)
                    {
                        if (col == COL_ATOM_X)
                        {
                            if (node.getCoordinates().getX() < 0)
                            {
                                sBuilder.append("   ");
                            }
                            else
                            {
                                sBuilder.append("    ");
                            }
                            sBuilder.append(String.format(Locale.US, "%1$.4f", node.getCoordinates().getX()));
                        }
                        else if (col == COL_ATOM_Y)
                        {
                            if (node.getCoordinates().getY() < 0)
                            {
                                sBuilder.append("   ");
                            }
                            else
                            {
                                sBuilder.append("    ");
                            }
                            sBuilder.append(String.format(Locale.US, "%1$.4f", node.getCoordinates().getY()));
                        }
                        else if (col == COL_ATOM_Z)
                        {
                            if (node.getCoordinates().getZ() < 0)
                            {
                                sBuilder.append("   ");
                            }
                            else
                            {
                                sBuilder.append("    ");
                            }
                            sBuilder.append(String.format(Locale.US, "%1$.4f", node.getCoordinates().getZ()));
                        }
                        else if (col == COL_ATOM_NAME)
                        {
                            position.put(node.getIdNode(), cRow);
                            //sBuilder.append(node.getIdNode() + "\t");
                            if (node.getLabel() == null)
                            {
                                sBuilder.append(" C   ");
                            }
                            else
                            {
                                sBuilder.append(" " + node.getLabel() + "   ");
                            }
                        }
                        else if (col == (COL_MAX_ATOM - 1))
                        {
                            sBuilder.append("0\n");
                        }
                        else
                        {
                            sBuilder.append("0  ");
                        }
                    }
                }
            }

			fWriter.write(sBuilder.toString());
			return position;
		}
		catch(IOException ioE)
		{
			logger.error(ioE, ioE);
		}

		return null;
	}

	private static void writeEdges(Writer fWriter, Graph graph, HashMap<Integer, Integer> positionNodes)
	{
		if(positionNodes != null)
		{
			try
			{
				StringBuilder sBuilder = new StringBuilder();

				for (Edge edge : graph.getSetEdges())
				{
					Node start = graph.getStartNode(edge);
					Node end = graph.getEndNode(edge);

                    if (start.getCoordinates() != null && end.getCoordinates() != null)
                    {
                        for (int col = 0; col < COL_MAX_BINDING; col++)
                        {
                            if (col == COL_BOND_FROM)
                            {
                                sBuilder.append(positionNodes.get(start.getIdNode()) + "\t");
                            }
                            else if (col == COL_BOND_TO)
                            {
                                sBuilder.append(positionNodes.get(end.getIdNode()) + "\t");
                            }
                            else if (col == COL_BOND_TYP)
                            {
                                sBuilder.append("1\t");
                            }
                            else if (col == (COL_MAX_BINDING - 1))
                            {
                                sBuilder.append("0\n");
                            }
                            else
                            {
                                sBuilder.append("0\t");
                            }
                        }
                    }
				}
                fWriter.write(sBuilder.toString());
            }
			catch (IOException ioE)
			{
				logger.error(ioE, ioE);
			}
		}
		else
		{
			logger.error("output.sdf fehlerhaft, kein Knoten gefunden!");
		}
	}

    private static void writeFooter(Writer fWriter)
    {
        try
        {
            fWriter.write("M\tEND\n$$$$\n");
        }
        catch (IOException ioE)
        {
            logger.error(ioE, ioE);
        }
    }

	/**
     *
     * @param filename
     */
	public static BidiMapAtom readSDF(String filename)
	{
		LineNumberReader reader = null;

        try
        {
            reader = new LineNumberReader(new FileReader(filename));
			Header header = readSDFHeader(reader);
			HashMap<Integer, Atom> atoms = null;
			BidiMapAtom graph = null;

			if(header != null)
			{
				reader = new LineNumberReader(new FileReader(filename));
				atoms = readSDFAtomBlock(reader, header);

				if(atoms != null)
				{
					reader = new LineNumberReader(new FileReader(filename));
					graph = readSDFBondBlock(reader, header, atoms);
				}

				return graph;
			}
			return null;
		}
        catch (FileNotFoundException e)
        {
            logger.error(e,e);
			return null;
		}
	}
	
	private static Header readSDFHeader(LineNumberReader  reader)
	{
        try
        {
            String line = "";
			Header header = new Header();

			while((line = reader.readLine()) != null && reader.getLineNumber() <= ROW_COUNTS)
			{
				if(reader.getLineNumber() == ROW_NAME)
				{
					line = line.trim().replaceAll(" +", "|");
					header.setName(line.split("\\|")[COL_FIRST]);
				}
				else if(reader.getLineNumber() == ROW_INFORMATION)
				{
					line = line.trim().replaceAll(" +", "|");
					header.setInfo(line.split("\\|")[COL_FIRST]);
				}
				else if(reader.getLineNumber() == ROW_COMMENT)
				{
					line = line.trim().replaceAll(" +", "|");
					header.setComment(line.split("\\|")[COL_FIRST]);
				}
				else if(reader.getLineNumber() == ROW_COUNTS)
				{
					line = line.trim().replaceAll(" +", "|");
					header.setCountAtoms(Integer.parseInt(line.split("\\|")[COL_COUNT_ATOM]));
					header.setCountBonds(Integer.parseInt(line.split("\\|")[COL_COUNT_BOND]));
				}
			}

			return header;
        }
        catch (IOException e)
        {
            logger.error(e,e);
			return null;
		}
	}

    private static HashMap<Integer, Atom> readSDFAtomBlock(LineNumberReader reader, Header header)
    {
        try
        {
            String line = "";
			int start = ROW_COUNTS;
			int end = ROW_COUNTS + header.getCountAtoms();
			HashMap<Integer, Atom> atoms = new HashMap<Integer, Atom>();

			while((line = reader.readLine()) != null)
			{
				// start < linenummer <= end
				if(reader.getLineNumber() > start && reader.getLineNumber() <= end)
				{
					line = line.trim().replaceAll(" +", "|");
					int id = reader.getLineNumber() - start;
					String name = line.split("\\|")[COL_ATOM_NAME];
					double x = Double.parseDouble(line.split("\\|")[COL_ATOM_X]);
					double y = Double.parseDouble(line.split("\\|")[COL_ATOM_Y]);
					double z = Double.parseDouble(line.split("\\|")[COL_ATOM_Z]);
					Koordinaten koords = new Koordinaten(x,y,z);
					Atom atom = new Atom(id, name, koords);
					atoms.put(id, atom);
				}
			}

            return atoms;
        }
        catch (IOException e)
        {
            logger.error(e,e);
			return null;
		}
	}

    private static BidiMapAtom readSDFBondBlock(LineNumberReader reader, Header header, HashMap<Integer, Atom> atoms)
    {
        try
        {
            String line = "";
			int start = ROW_COUNTS + header.getCountAtoms();
			int end = ROW_COUNTS + header.getCountAtoms() + header.getCountBonds();
			BidiMapAtom graph = new BidiMapAtom();

            while ((line = reader.readLine()) != null)
            {
				// start < linenummer <= end
				if(reader.getLineNumber() > start && reader.getLineNumber() <= end)
				{
					line = line.trim().replaceAll(" +", "|");
					int von = Integer.parseInt(line.split("\\|")[COL_BOND_FROM]);
					int bis = Integer.parseInt(line.split("\\|")[COL_BOND_TO]);
					int typ = Integer.parseInt(line.split("\\|")[COL_BOND_TYP]);
					Atom atomVon = atoms.get(von);
					Atom atomBis = atoms.get(bis);
					Bindung bindung = new Bindung();
					bindung.setBTyp(typ);

                    graph.putEntry(new MultiKeyAtom(atomVon, atomBis), bindung);
                }
			}

            return graph;
        }
        catch (IOException e)
        {
            logger.error(e,e);
			return null;
		}
	}
	
	@SuppressWarnings("unused")
	private static int countSDFLines(LineNumberReader reader)
	{
        try
        {
            int count = 0;

            while (reader.readLine() != null)
            {
				count++;
			}

            return count;
        }
        catch (IOException e)
        {
            logger.error(e,e);
			return 0;
        }
    }

    /**
     * Innere Klasse fuer Header
     *
     * @author Mika
     */
    static class Header
    {
        int countAtoms;
        int countBonds;
        String name;
        String comment;
        String info;


        public Header()
        {
            this.countAtoms = 0;
            this.countBonds = 0;
            this.comment = "";
            this.info = "";
        }

        /*
         * Getter
         */
        public int getCountAtoms()
        {
            return this.countAtoms;
        }

        /*
         * Setter
         */
        public void setCountAtoms(int atoms)
        {
            this.countAtoms = atoms;
        }

        public int getCountBonds()
        {
            return this.countBonds;
        }

        public void setCountBonds(int bonds)
        {
            this.countBonds = bonds;
        }

        public String getName()
        {
            return this.name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getComment()
        {
            return this.comment;
        }

        public void setComment(String comment)
        {
            this.comment = comment;
        }

        public String getInfo()
        {
            return this.info;
        }

        public void setInfo(String info)
        {
            this.info = info;
        }
    }
}

