package logic;

import beans.geometry.Koordinaten;
import beans.geometry.Kreis;
import beans.geometry.Kugel;
import beans.graph.Edge;
import beans.graph.Graph;
import beans.graph.MultiKeyNode;
import beans.graph.Node;
import exceptions.NoIntersectionException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by Mika on 09.11.2016.
 */
public class Geometry
{
    private Geometry()
    {
    }

    public static Kreis calcIntersectionSphereSphere(Kugel sphereOne, Kugel sphereTwo) throws NoIntersectionException
    {
        /*---------------------------------------
         *
         *           Daten initialisierung
         *
         --------------------------------------*/

        /* Daten Kugel_1 */
        Koordinaten midOne = sphereOne.getMidPoint();
        double radOne = sphereOne.getRadius();

        /* Daten Kugel_2 */
        Koordinaten midTwo = sphereTwo.getMidPoint();
        double radTwo = sphereTwo.getRadius();

        /* Distanz */
        double distance = midOne.calcDistance(midTwo);

        /*---------------------------------------
         *
         *               Berechnung
         *
         --------------------------------------*/

        if (distance + Math.min(radOne, radTwo) > Math.max(radOne, radTwo))
        {
            if (radOne + radTwo > distance)
            {
                double factorH = 0.5 + ((Math.pow(radOne, 2) - Math.pow(radTwo, 2)) / (2 * Math.pow(distance, 2)));

                /* Mittelpunkt Kreis */
                Koordinaten centerOfIntersectionCircle = sumCoordinates(midOne, scalarMultiplication(factorH, subCoordinates(midTwo, midOne)));

                /* Radius Kreis */
                double radiusOfIntersectionCircle = Math.sqrt(Math.pow(radOne, 2) - Math.pow(factorH * distance, 2));

                /* Normalenvektor */
                Koordinaten normVec = scalarDivision(subCoordinates(midTwo, midOne), distance);

                return new Kreis(radiusOfIntersectionCircle, centerOfIntersectionCircle, normVec);
            }
            else
            {
                throw new NoIntersectionException();
            }
        }
        else
        {
            throw new NoIntersectionException();
        }
    }

    /**
     * a + b.
     *
     * @param a
     * @param b
     * @return
     */
    public static Koordinaten sumCoordinates(Koordinaten a, Koordinaten b)
    {
        double xNew = a.getX() + b.getX();
        double yNew = a.getY() + b.getY();
        double zNew = a.getZ() + b.getZ();

        return new Koordinaten(xNew, yNew, zNew);
    }

    /**
     * factor * vector
     *
     * @param factor
     * @param vector
     * @return
     */
    public static Koordinaten scalarMultiplication(double factor, Koordinaten vector)
    {
        double xNew = factor * vector.getX();
        double yNew = factor * vector.getY();
        double zNew = factor * vector.getZ();

        return new Koordinaten(xNew, yNew, zNew);
    }

    /**
     * a - b.
     *
     * @param a
     * @param b
     * @return
     */
    public static Koordinaten subCoordinates(Koordinaten a, Koordinaten b)
    {
        double xNew = a.getX() - b.getX();
        double yNew = a.getY() - b.getY();
        double zNew = a.getZ() - b.getZ();

        return new Koordinaten(xNew, yNew, zNew);
    }

    /**
     * Vector / divisor
     *
     * @param coordinates
     * @param divisor
     * @return
     */
    public static Koordinaten scalarDivision(Koordinaten coordinates, double divisor)
    {
        try
        {
            double x = coordinates.getX() / divisor;
            double y = coordinates.getY() / divisor;
            double z = coordinates.getZ() / divisor;

            return new Koordinaten(x, y, z);
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    /**
     * Prüft, mit welcher (absoluten!) Abweichung ein Punkt von der Oberfläche einer Kugel entfernt liegt
     *
     * @param coords
     * @param sphere
     * @return
     */
    public static double deviationOfPointOnSphereSurface(Koordinaten coords, Kugel sphere)
    {
        double distance = coords.calcDistance(sphere.getMidPoint());
        return Math.abs(distance - sphere.getRadius());
    }

    /**
     * Berechnet die Schnittpunkte einer Kugel mit einem 3D-Kreis (Schnittebene mit Mittelpunkt und Radius)
     *
     * @param sphere
     * @param circle
     * @return
     * @throws NoIntersectionException
     */
    public static ArrayList<Koordinaten> calcIntersectionSphereCircle(Kugel sphere, Kreis circle) throws NoIntersectionException
    {
        Koordinaten circleNorm = circle.getNormalVector();
        double distanceFromPlaneToCenterOfSphere = scalarProduct(circleNorm, subCoordinates(circle.getMidPoint(), sphere.getMidPoint()));
        double radiusOfIntersectionPlaneWithSphere;
        double distanceFromCircleCenterToIntersectionCircleCenter;
        double factorH;
        double radiusOfIntersectionCircle;
        ArrayList<Koordinaten> out = new ArrayList<>();

        // Kreis und Kugel schneiden sich nicht:
        if (Math.abs(distanceFromPlaneToCenterOfSphere) < sphere.getRadius())
        {
            /**
             * Bestimmen des Schnittkreises der Kugel mit der Ebene des Kreises
             */

            /* Mittelpunkt */
            Koordinaten centerOfIntersectionPlaneWithSphere = sumCoordinates(sphere.getMidPoint(), scalarMultiplication(distanceFromPlaneToCenterOfSphere, circleNorm));

            /* Radius */
            radiusOfIntersectionPlaneWithSphere = Math.sqrt(Math.pow(sphere.getRadius(), 2) - Math.pow(distanceFromPlaneToCenterOfSphere, 2));


            /**
             * Schnitt des Kreises mit dem Schnittkreis der Ebene des Kreises mit der Kugel
             */
            distanceFromCircleCenterToIntersectionCircleCenter = circle.getMidPoint().calcDistance(centerOfIntersectionPlaneWithSphere);
            factorH = 0.5 + ((Math.pow(circle.getRadius(), 2) - Math.pow(radiusOfIntersectionPlaneWithSphere, 2)) / (2 * Math.pow(distanceFromCircleCenterToIntersectionCircleCenter, 2)));

            if (circle.getRadius() + radiusOfIntersectionPlaneWithSphere > distanceFromCircleCenterToIntersectionCircleCenter)
            {
                if (Math.abs(circle.getRadius() - radiusOfIntersectionPlaneWithSphere) < distanceFromCircleCenterToIntersectionCircleCenter)
                {
                    /* Mittelpunkt */
                    Koordinaten centerOfIntersectionCircle = sumCoordinates(circle.getMidPoint(), scalarMultiplication(factorH, subCoordinates(centerOfIntersectionPlaneWithSphere, circle.getMidPoint())));

                    /* Radius */
                    radiusOfIntersectionCircle = Math.sqrt(Math.pow(circle.getRadius(), 2) - Math.pow(factorH * distanceFromCircleCenterToIntersectionCircleCenter, 2));

                    /**
                     * Bestimmung der beiden Schnittpunkte
                     */
                    /* Tangenten-Vektor */
                    Koordinaten tangentVector = normalize(crossProduct(subCoordinates(centerOfIntersectionPlaneWithSphere, circle.getMidPoint()), circleNorm));

                    /* Schnittpunkte bestimmen */
                    out.add(subCoordinates(centerOfIntersectionCircle, scalarMultiplication(radiusOfIntersectionCircle, tangentVector)));
                    out.add(sumCoordinates(centerOfIntersectionCircle, scalarMultiplication(radiusOfIntersectionCircle, tangentVector)));
                    return out;
                }
            }
        }
        throw new NoIntersectionException();
    }

    /**
     * Scalar product
     *
     * @param a
     * @param b
     * @return
     */
    public static double scalarProduct(Koordinaten a, Koordinaten b)
    {
        double scalarProduct = (a.getX() * b.getX()) +
                (a.getY() * b.getY()) +
                (a.getZ() * b.getZ());
        return scalarProduct;
    }

    /**
     * Vektor normalisieren
     *
     * @param coordinates
     * @return
     */
    public static Koordinaten normalize(Koordinaten coordinates)
    {
        double x = coordinates.getX();
        double y = coordinates.getY();
        double z = coordinates.getZ();

        double divisor = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));

        return scalarDivision(coordinates, divisor);
    }

    /**
     * Cross product
     * a x b
     *
     * @param a
     * @param b
     * @return
     */
    public static Koordinaten crossProduct(Koordinaten a, Koordinaten b)
    {
        double crossX = (a.getY() * b.getZ()) - (a.getZ() * b.getY());
        double crossY = (a.getZ() * b.getX()) - (a.getX() * b.getZ());
        double crossZ = (a.getX() * b.getY()) - (a.getY() * b.getX());

        return new Koordinaten(crossX, crossY, crossZ);
    }

    /**
     * Nächste Nachbarn finden
     *
     * @param node
     * @param graph
     * @return
     */
    public static ArrayList<Node> findNeighboursNode(Node node, HashSet<Node> targetSet, Graph graph) /* TODO: Move to CombinatorialEmbedding class */
    {
        HashSet<Node> nodes = new HashSet<>();
        MultiKeyNode mkn;

        for (Map.Entry<MultiKeyNode, HashSet<Edge>> entry : graph.getGraphMap().getEntrySet())
        {
            mkn = entry.getKey();
            if (mkn.contains(node))
            {
                if (!graph.getSetEdges(mkn.getStartNode(), mkn.getEndNode()).isEmpty())
                {
                    if (mkn.getStartNode() == node)
                    {
                        if (targetSet.contains(mkn.getEndNode()))
                        {
                            nodes.add(mkn.getEndNode());
                        }
                    }
                    else if (mkn.getEndNode() == node)
                    {
                        if (targetSet.contains(mkn.getStartNode()))
                        {
                            nodes.add(mkn.getStartNode());
                        }
                    }
                }
            }
        }
        return new ArrayList<>(nodes);
    }
}
