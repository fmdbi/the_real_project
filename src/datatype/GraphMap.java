package datatype;

import beans.graph.Edge;
import beans.graph.MultiKeyNode;
import beans.graph.Node;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;


/**
 * Speichert die Zuordnung von MultiKeyNodes (bestehend aus Start- und End-Node)
 * und jeweils einer Liste von Edges, die Start- und End-Node verbinden
 *
 * Created by Mika on 01.10.2016.
 */
public class GraphMap
{
    private static final Logger logger = Logger.getLogger(GraphMap.class);

    private HashMap<MultiKeyNode, HashSet<Edge>> mapNodeEdge = new HashMap<>();
    private HashMap<Edge, MultiKeyNode> mapEdgeNode = new HashMap<>();
    private HashSet<Node> nodeSet = new HashSet<>();
    private HashSet<Edge> edgeSet = new HashSet<>();

    /**
     * Default-Konstruktor
     */
    public GraphMap()
    {
    }

    /**
     * Entfernt Edge
     *
     * @param edge
     */
    public void removeEdge(Edge edge)
    {
        this.edgeSet.remove(edge);
        this.mapNodeEdge.get(this.getKeyByValue(edge)).remove(edge);
        this.mapEdgeNode.remove(edge);
    }

    /**
     * Zugehörigen MultiKeyNode (Key) zu gegebener Edge (Value) zurückgeben
     *
     * @param value Edge
     * @return MultiKeyNode
     */
    public MultiKeyNode getKeyByValue(Edge value) // fixme performance
    {
        /*
        for (Entry<MultiKeyNode, HashSet<Edge>> entry : this.mapNodeEdge.entrySet())
        {
            if (entry.getValue().contains(value))
            {
                return entry.getKey();
            }
        }
        return null;
        */
        return this.mapEdgeNode.get(value);
    }

    /**
     * Tauscht Startknoten und Endknoten des MultiKeyNode-Objekts
     * Entfernt Edge aus der Menge der Edges des MKN der Ursprungsrichtung,
     * fügt Edge zur Menge der Edges des MKN der Rückrichtung hinzu
     * (Edge-Covering: Lowest-Level)
     *
     * @param edge
     */
    public void changeDirection(Edge edge)
    {
        MultiKeyNode nodes = this.getKeyByValue(edge);

        Node startNode = nodes.getStartNode();
        Node endNode = nodes.getEndNode();

        this.mapNodeEdge.get(this.getKeyByValue(edge)).remove(edge);
        this.mapEdgeNode.remove(edge);
        this.putEntry(new MultiKeyNode(endNode, startNode), edge);
    }

    /**
     * Edge hinzufügen
     * Bei bereits vorhandenem MKN wird Edge diesem hinzugefügt,
     * sonst Anlegen eines neuen MKN
     *
     * @param mkn   MultiKeyNode
     * @param edge Edge
     */
    public void putEntry(MultiKeyNode mkn, Edge edge) // TODO: Mehrfaches Hinzufügen derselben Edge verbieten
    {
        this.edgeSet.add(edge);
        this.nodeSet.add(mkn.getStartNode());
        this.nodeSet.add(mkn.getEndNode());

        if (mapNodeEdge.containsKey(mkn))
        {
            this.mapNodeEdge.get(mkn).add(edge);
        }
        else
        {
            HashSet<Edge> edges = new HashSet<>();
            edges.add(edge);
            this.mapNodeEdge.put(mkn, edges);
        }

        if (mapEdgeNode.containsKey(edge))
        {
            this.mapEdgeNode.remove(edge);
            this.mapEdgeNode.put(edge, mkn);
        }
        else
        {
            this.mapEdgeNode.put(edge, mkn);
        }

    }

    /**
     * Entry-Set für die Iteration über Hashmap-Zuordnung von MKNs zu Edge-Mengen
     *
     * @return EntrySet von MKN -> [Edges]
     */
    public Set<Entry<MultiKeyNode, HashSet<Edge>>> getEntrySet()
    {
        return this.mapNodeEdge.entrySet();
    }

    /*
     * Getter für HashMap der NodeNode-Edge Beziehung
     */
    public HashMap<MultiKeyNode, HashSet<Edge>> getMapNodeEdge()
    {
        return mapNodeEdge;
    }
    /**
     * Gibt alle Knoten des Graphs zurueck
     *
     * @return
     */
    public HashSet<Node> getSetNodes()
    {
        return nodeSet;
    }

    /**    public HashSet<Edge> getValueByKey(MultiKeyNode key)
     {
     return this.mapNodeEdge.get(key);
     }

     * Gibt alle Kanten des Graphs zurueck
     *
     * @return
     */
    public HashSet<Edge> getSetEdges()
    {
        return edgeSet;
    }

    /**
     * Gibt alle Kanten des Knotens node zurueck
     *
     * @param node
     * @return
     */
    public HashSet<Edge> getSetEdges(Node node)
    {
        HashSet<Edge> out = new HashSet<>();

        for (Entry<MultiKeyNode, HashSet<Edge>> entry : this.mapNodeEdge.entrySet())
        {
            if (entry.getKey().contains(node))
            {
                for (Edge edge : entry.getValue())
                {
                    out.add(edge);
                }
            }
        }
        return out;
    }

    /**
     * Edges zwischen zwei Nodes zurückgeben
     *
     * @param node1
     * @param node2
     * @return
     */
    public HashSet<Edge> getSetEdges(Node node1, Node node2)
    {
        HashSet<Edge> out = new HashSet<>();

        MultiKeyNode mkn = new MultiKeyNode(node1, node2);
        HashSet<Edge> firstDir = mapNodeEdge.get(mkn);
        if (firstDir != null)
        {
            out.addAll(firstDir);
        }

        mkn = new MultiKeyNode(node2, node1);
        HashSet<Edge> secondDir = mapNodeEdge.get(mkn);
        if (secondDir != null)
        {
            out.addAll(secondDir);
        }

        return out;
    }
}
