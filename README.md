# README #

Usage:

```
java -jar molDistEmbed.jar -i <String> -o <String> -r <Float> -e <String>
```
* -i: 	Filename of the input sdf file (should contain complete molecule files. Used to simulate real-data input)
* -o: 	Suffix of the output sdf file (subgraph-id will be attached to the front)
* -r: 	How many distances (percent; 0.0 - 1.0) should be removed from input molecule to simulate real-data input?
* -e: 	Which graph embedder should be used ("ff" or "combi")?

Example:
```
java -jar molDistEmbed.jar -i caffeine.sdf -o caffeine_dist_combi.sdf -r 0.5 -e combi
```

[Download executable](https://bitbucket.org/fmdbi/the_real_project/raw/master/out/artifacts/molDistEmbed/molDistEmbed.jar)

The output SDFs are best viewed with jMol (http://jmol.sourceforge.net/). It deduces bonds and colors the atoms.

**MolDistEmbed SDF output (caffeine, viewed in jMol):**
![combinatoricEmbedding.jpg](https://bitbucket.org/repo/XaRyXd/images/2802637536-combinatoricEmbedding.jpg)

**Corresponding pubchem SDF input (caffeine, viewed in jMol):**
![Structure3D_CID_2519.jpg](https://bitbucket.org/repo/XaRyXd/images/525140902-Structure3D_CID_2519.jpg)