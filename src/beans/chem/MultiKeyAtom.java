package beans.chem;

import org.apache.log4j.Logger;

public class MultiKeyAtom
{
	private static final Logger logger = Logger.getLogger(MultiKeyAtom.class);
	private Atom keyFirst;
	private Atom keySecond;
  
	public MultiKeyAtom(Atom keyOne, Atom keyTwo)
	{
		if(validate(keyOne, keyTwo))
		{
			this.keyFirst = keyOne;
			this.keySecond = keyTwo;
		}
	}

    /**
     * Validierung der Konstruktorparameter
     *
     * @param keyOne
     * @param keyTwo
     * @return
     */
    private boolean validate(Atom keyOne, Atom keyTwo)
    {
        if (keyOne == null)
        {
            logger.error("(Class MultiKeyAtom) Key_One ist NULL!");
            return false;
        }
        else if (keyTwo == null)
        {
            logger.error("(Class MultiKeyAtom) Key_Two ist NULL!");
            return false;
        }
        else
        {
            //LOG.info("(Class MultiKeyAtom) Keys valide.");
            return true;
        }
    }

	@Override
	public boolean equals(Object obj)
    {
        if (!(obj instanceof MultiKeyAtom))
        {
            return false;
		}
		else
        {
            boolean flag = false;
			Atom atomFirst 	= ((MultiKeyAtom) obj).getFirstKey();
			Atom atomSecond	= ((MultiKeyAtom) obj).getSecondKey();

            if (this.keyFirst.equals(atomFirst) && this.keySecond.equals(atomSecond))
            {
                flag = true;
            }
            else
            {
                flag = this.keySecond.equals(atomFirst) && this.keyFirst.equals(atomSecond);
            }

            return flag;
        }
    }

    public Atom getFirstKey()
	{
		return this.keyFirst;
	}

	public Atom getSecondKey()
	{
		return this.keySecond;
    }
}