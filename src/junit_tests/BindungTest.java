package junit_tests;

import static com.sun.xml.internal.ws.dump.LoggingDumpTube.Position.Before;
import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import beans.chem.Bindung;

public class BindungTest
{
	private static final Logger logger = Logger.getLogger(BindungTest.class);

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSwitch() 
	{
		Bindung bindung = new Bindung();
		bindung.switchBindung();
		assertEquals(-1, bindung.getRichtung());
	}

}
