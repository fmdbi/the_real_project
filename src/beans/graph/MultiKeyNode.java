package beans.graph;

import org.apache.log4j.Logger;

/**
 * Created by Mika on 01.10.2016.
 */
public class MultiKeyNode
{
    private static final Logger logger = Logger.getLogger(MultiKeyNode.class);
    private Node startNode;
    private Node endNode;

    public MultiKeyNode(Node keyOne, Node keyTwo)
    {
        if (validate(keyOne, keyTwo))
        {
            this.startNode = keyOne;
            this.endNode = keyTwo;
        }
    }

    /**
     * Validierung der Konstruktorparameter
     *
     * @param keyOne
     * @param keyTwo
     * @return
     */
    private boolean validate(Node keyOne, Node keyTwo)
    {
        if (keyOne == null)
        {
            logger.error("(Class MultiKeyNode) Key_One ist NULL!");
            return false;
        }
        else if (keyTwo == null)
        {
            logger.error("(Class MultiKeyNode) Key_Two ist NULL!");
            return false;
        }
        else
        {
            //logger.info("(Class MultiKeyNode) Keys valide.");
            return true;
        }
    }

    @Override
    public int hashCode()
    {
        return this.getStartNode().hashCode() / 2 + this.getEndNode().hashCode() / 3;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof MultiKeyNode))
        {
            return false;
        }
        else
        {
            Node nodeFirst = ((MultiKeyNode) obj).getStartNode();
            Node nodeSecond = ((MultiKeyNode) obj).getEndNode();

            return (this.startNode == nodeFirst) && (this.endNode == nodeSecond);
        }
    }

    public Node getEndNode()
    {
        return this.endNode;
    }

    public Node getStartNode()
    {
        return this.startNode;
    }

    public boolean contains(Node node)
    {
        if (this.getStartNode().equals(node))
        {
            return true;
        }
        return this.getEndNode().equals(node);
    }

    public String toString()
    {
        return "nodeStart: " + this.startNode.getIdNode() + " nodeEnde: " + this.getEndNode().getIdNode();
    }
}
