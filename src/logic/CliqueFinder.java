package logic;

import beans.graph.Graph;
import beans.graph.Node;

import java.util.ArrayList;

/**
 * Finds 4- or 3-cliques
 */
public class CliqueFinder
{
    private CliqueFinder()
    {
    }

    /**
     * Finds 4- or 3-cliques
     *
     * @param graph
     * @param kClique k = {3, 4}
     * @return List of all k-cliques
     */
    public static ArrayList<ArrayList<Node>> findKClique(Graph graph, int kClique)
    {
        ArrayList<ArrayList<Node>> cliques = new ArrayList<>();

        for (Node node1 : graph.getSetNodes())
        {
            for (Node node2 : graph.getSetNodes())
            {
                for (Node node3 : graph.getSetNodes())
                {
                    if (kClique == 4)
                    {
                        for (Node node4 : graph.getSetNodes())
                        {
                            if (graph.getSetEdges(node1, node2).size() == 0 || node1 == node2)
                            {
                                continue;
                            }
                            if (graph.getSetEdges(node1, node3).size() == 0 || node1 == node3)
                            {
                                continue;
                            }
                            if (graph.getSetEdges(node1, node4).size() == 0 || node1 == node4)
                            {
                                continue;
                            }
                            if (graph.getSetEdges(node2, node3).size() == 0 || node2 == node3)
                            {
                                continue;
                            }
                            if (graph.getSetEdges(node2, node4).size() == 0 || node2 == node4)
                            {
                                continue;
                            }
                            if (graph.getSetEdges(node3, node4).size() == 0 || node3 == node4)
                            {
                                continue;
                            }

                            ArrayList<Node> clique = new ArrayList<>();

                            clique.add(node1);
                            clique.add(node2);
                            clique.add(node3);
                            clique.add(node4);

                            cliques.add(clique);
                        }
                    }
                    else if (kClique == 3)
                    {
                        if (graph.getSetEdges(node1, node2).size() == 0 || node1 == node2)
                        {
                            continue;
                        }
                        if (graph.getSetEdges(node1, node3).size() == 0 || node1 == node3)
                        {
                            continue;
                        }
                        if (graph.getSetEdges(node2, node3).size() == 0 || node2 == node3)
                        {
                            continue;
                        }

                        ArrayList<Node> clique = new ArrayList<>();

                        clique.add(node1);
                        clique.add(node2);
                        clique.add(node3);

                        cliques.add(clique);
                    }
                    else
                    {
                        return cliques;
                    }
                }
            }
        }
        return cliques;
    }
}
