package logic;

import beans.geometry.Koordinaten;
import beans.geometry.Kreis;
import beans.geometry.Kugel;
import beans.graph.Edge;
import beans.graph.Graph;
import beans.graph.Node;
import exceptions.NoIntersectionException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class CombinatorialEmbedding
{
    private static final Logger logger = Logger.getLogger(CombinatorialEmbedding.class);

    private final static double EPSILON = 0.0001; /* TODO: Evaluate epsilon */
    private static HashMap<Node, ArrayList<Koordinaten>> nodesCoordsMap = new HashMap<>(); // TODO: Nutzen ;-)

    /**
     * Tries to realize a given graph via combinatorial embedding
     *
     * @param graph
     * @throws Exception
     */
    public static boolean embedGraph(Graph graph)
    {
        ArrayList<Node> nodes = new ArrayList<>();
        nodes.addAll(graph.getSetNodes());

        if (nodes.size() < 3)
        {
            logger.error("Combinatorial embedding is not (yet) implemented for graphs with |nodes| < 3."); // TODO
        }
        else
        {
            /**
             * Finde eine 3-Clique
             */
            ArrayList<ArrayList<Node>> threeCliques = CliqueFinder.findKClique(graph, 3);

            if (threeCliques.size() < 1)
            {
                logger.error("No 3-clique found.");
            }
            else
            {
                boolean embeddingHasWorked = false;

                int i = 0;
                while (!embeddingHasWorked && i < threeCliques.size())
                {
                    ArrayList<Node> threeClique = threeCliques.get(i);

                    /* Einen "Primer" aus drei Nodes einbetten */
                    if (primerEmbed(threeClique, graph))
                    {

                        /*
                        /* Die restlichen Nodes einbetten */
                        /*
                        /* pool vorbereiten */
                        HashSet<Node> poolNodes = new HashSet<>();
                        poolNodes.addAll(nodes);
                        poolNodes.removeAll(threeClique);

                        /* embedded vorbereiten */
                        HashSet<Node> embeddedNodes = new HashSet<>();
                        embeddedNodes.addAll(threeClique);

                        /* Rekursiv einbetten */
                        embeddingHasWorked = polyRecEmbed(embeddedNodes, poolNodes, graph);
                    }
                    i++;
                }
                return embeddingHasWorked;
            }
        }
        return false;
    }

    /**
     * Embeds a primer of three nodes.
     * <p>
     * 1st: trivial -> (0, 0, 0)
     * 2nd: almost trivial -> (0, 0 + dist, 0)
     * 3d: intersect of spheres of distances to 1 and 2, set to "down under" the intersection circle
     *
     * @param threeClique
     * @param graph
     * @return
     */
    private static boolean primerEmbed(ArrayList<Node> threeClique, Graph graph)
    {
        Node firstNode = threeClique.get(0);
        Node secondNode = threeClique.get(1);
        Node thirdNode = threeClique.get(2);

        /**
         * FirstNode einbetten (frei)
         */
        firstNode.setCoordinates(new Koordinaten(0, 0, 0)); // Position fixed

        /**
         * SecondNode einbetten (frei, mit fester Distanz zu firstNode)
         */
        double dist21 = 0;

        for (Edge edge : graph.getSetEdges(secondNode, firstNode))
        {
            dist21 = edge.getLength();
            break;
        }

        secondNode.setCoordinates(new Koordinaten(0 + dist21, 0, 0)); // Position fixed

        /**
         * ThirdNode einbetten (auf Schnittkreis von firstNode und secondNode)
         */
        double dist31 = 0;
        double dist32 = 0;

        for (Edge edge : graph.getSetEdges(thirdNode, firstNode))
        {
            dist31 = edge.getLength();
            break;
        }
        for (Edge edge : graph.getSetEdges(thirdNode, secondNode))
        {
            dist32 = edge.getLength();
            break;
        }

        /* Schnittkreis für thirdNode, zwischen secondNode und firstNode */
        Kreis intersectionCircle3_21;
        try
        {
            intersectionCircle3_21 = Geometry.calcIntersectionSphereSphere(
                    new Kugel(dist31, firstNode.getCoordinates()),
                    new Kugel(dist32, secondNode.getCoordinates())
            );
        }
        catch (Exception e)
        {
            return false;
        }

        /* "Unter" den beiden anderen Nodes positionieren */
        thirdNode.setCoordinates(new Koordinaten(
                intersectionCircle3_21.getMidPoint().getX(),
                intersectionCircle3_21.getMidPoint().getY() - intersectionCircle3_21.getRadius(),
                intersectionCircle3_21.getMidPoint().getZ()) // Position fixed
        );
        return true;
    }

    /**
     * Embedding of nodes based on already embedded 'primer'.
     * Uses recursive backtracking.
     *
     * @param embeddedNodes
     * @param poolNodes
     * @param graph
     * @return
     */
    private static boolean polyRecEmbed(HashSet<Node> embeddedNodes, HashSet<Node> poolNodes, Graph graph)
    {
        if (poolNodes.size() == 0)
        {
            return true;
        }

        Node node = null;
        ArrayList<Node> embeddedNeighbours = new ArrayList<>();

        for (Node loopNode : poolNodes)
        {
            embeddedNeighbours = Geometry.findNeighboursNode(loopNode, embeddedNodes, graph);

            if (embeddedNeighbours.size() > 2)
            {
                if (graph.testFourConnectivity(loopNode)) // Wenn Node nur 3-connected: übrig lassen,später zweideutig einbetten
                {
                    node = loopNode;
                    break;
                }
            }
        }

        if (node == null) // Nur noch (<4)-connected Nodes übrig, Einbettung zweideutig
        {
            for (Node loopNode : poolNodes)
            {
                embeddedNeighbours = Geometry.findNeighboursNode(loopNode, embeddedNodes, graph);

                if (embeddedNeighbours.size() > 2)
                {
                    node = loopNode;
                    break;
                }
            }
        }

        if (node != null)
        {
            ArrayList<Koordinaten> coordListNthNode;

            /**
             * NthNode einbetten
             */
            double distN1 = 0;
            double distN2 = 0;
            double distN3 = 0;

            Node firstNode = embeddedNeighbours.get(0);
            Node secondNode = embeddedNeighbours.get(1);
            Node thirdNode = embeddedNeighbours.get(2);

            for (Edge edge : graph.getSetEdges(node, firstNode))
            {
                distN1 = edge.getLength();
                break;
            }
            for (Edge edge : graph.getSetEdges(node, secondNode))
            {
                distN2 = edge.getLength();
                break;
            }
            for (Edge edge : graph.getSetEdges(node, thirdNode))
            {
                distN3 = edge.getLength();
                break;
            }

            /* Schnittkreis für NthNode, zwischen secondNode und firstNode */
            try
            {
                Kreis intersectionCircleN_21;
                intersectionCircleN_21 = Geometry.calcIntersectionSphereSphere(
                        new Kugel(distN1, firstNode.getCoordinates()),
                        new Kugel(distN2, secondNode.getCoordinates())
                );

                /* Zwei Schnittpunkte für NthNode auf obigem Schnittkreis */
                try
                {
                    coordListNthNode = Geometry.calcIntersectionSphereCircle(
                            new Kugel(distN3, thirdNode.getCoordinates()),
                            intersectionCircleN_21);

                    /**
                     * Falls es vier Edges zu bereits eingebetteten Nodes gibt,
                     * kann 'eindeutig' eingebettet werden.
                     */
                    if (embeddedNeighbours.size() > 3)
                    {
                        HashMap<Koordinaten, Double> coordDeviations = new HashMap<>();

                        for (Koordinaten coord : coordListNthNode)
                        {
                            double distN4 = 0;
                            Node fourthNode = embeddedNeighbours.get(3);
                            for (Edge edge : graph.getSetEdges(node, fourthNode))
                            {
                                distN4 = edge.getLength();
                                break;
                            }
                            coordDeviations.put(coord, Geometry.deviationOfPointOnSphereSurface(coord, new Kugel(distN4, fourthNode.getCoordinates())));
                        }

                        /*
                        Bestimme Coordinate mit der geringsten Abweichung zur Kugeloberfläche und prüfe, ob
                        der Wert innerhalb der Toleranz (EPSILON) liegt
                         */
                        Koordinaten bestFittingCoord;

                        if (coordDeviations.get(coordListNthNode.get(0)) < coordDeviations.get(coordListNthNode.get(1)))
                        {
                            bestFittingCoord = coordListNthNode.get(0);
                        }
                        else
                        {
                            bestFittingCoord = coordListNthNode.get(1);
                        }

                        if (coordDeviations.get(bestFittingCoord) < CombinatorialEmbedding.EPSILON)
                        {
                            coordListNthNode = new ArrayList<>();
                            coordListNthNode.add(bestFittingCoord);
                        }
                        else
                        {
                            return false;
                        }
                    }

                    /* Wenn Node nicht 4-connected: Merke beide möglichen Coordinates */
                    if (!graph.testFourConnectivity(node))
                    {
                        CombinatorialEmbedding.nodesCoordsMap.put(node, coordListNthNode);
                    }

                }
                catch (NoIntersectionException e)
                {
                    return false;
                }
            }
            catch (NoIntersectionException e)
            {
                return false;
            }

            for (Koordinaten coords : coordListNthNode)
            {
                node.setCoordinates(coords);

                HashSet<Node> poolNodesNew = new HashSet<>();
                poolNodesNew.addAll(poolNodes);
                poolNodesNew.remove(node);

                HashSet<Node> embeddedNodesNew = new HashSet<>();
                embeddedNodesNew.addAll(embeddedNodes);
                embeddedNodesNew.add(node);

                if (polyRecEmbed(embeddedNodesNew, poolNodesNew, graph))
                {
                    return true;
                }
            }
        }
        return false;
    }

}
