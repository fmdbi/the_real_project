package beans.graph;

import org.apache.log4j.Logger;

public abstract class GraphElement
{
    private static final Logger logger = Logger.getLogger(GraphElement.class);
    protected Component componentTag = null;

    /**
     * Getter Component-tag
     *
     * @return Component, die als letzte zugewiesen wurde
     */
    public Component getComponentTag()
    {
        return componentTag;
    }

}
