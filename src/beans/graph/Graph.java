package beans.graph;

import beans.chem.Atom;
import beans.chem.Molekuel;
import datatype.GraphMap;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

public class Graph
{
    private static final Logger logger = Logger.getLogger(Graph.class);
    private static int counter = 0;
    private String idGraph;
    private GraphMap graphMap = new GraphMap();

    /**
     * Default-Konstruktor
     */
    public Graph()
    {
        counter++;
        idGraph = "Graph_" + counter;
    }

    /**
     * Konstruktor, der aus einem Input-Molekül einen vollständigen Graphen erzeugt.
     *
     * @param molecule
     */
    public Graph(Molekuel molecule)
    {
        counter++;
        idGraph = "Graph_" + counter;

        HashSet<Atom> atome = molecule.getSetAtome();
        HashMap<Atom, Node> atomNodeMap = new HashMap<>();

        for (Atom atom : atome)
        {
            Node node = new Node();
            node.setLabel(atom.getElement());
            atomNodeMap.put(atom, node);
        }

        for (Atom atom1 : atome)
        {
            for (Atom atom2 : atome)
            {
                if (atom1 != atom2)
                {
                    if (this.getSetEdges(atomNodeMap.get(atom1), atomNodeMap.get(atom2)).size() == 0)
                    {
                        double distance = atom1.getCoordinates().calcDistance(atom2.getCoordinates());
                        Edge newEdge = new Edge(Edge.count, distance, false, false);
                        this.addEdge(newEdge, atomNodeMap.get(atom1), atomNodeMap.get(atom2));
                    }
                }
            }
        }
    }

    /**
     * @param node1
     * @param node2
     * @return
     */
    public HashSet<Edge> getSetEdges(Node node1, Node node2)
    {
        return this.graphMap.getSetEdges(node1, node2);
    }

    /**
     * Edge hinzufügen
     *
     * @param edge
     * @param startNode
     * @param endNode
     */
    public void addEdge(Edge edge, Node startNode, Node endNode)
    {
        this.graphMap.putEntry(new MultiKeyNode(startNode, endNode), edge);
    }

    /**
     * Konstruktor mit Graph-ID
     *
     * @param id Beschriftung des Graphen
     */
    public Graph(String id)
    {
        this.idGraph = id;
    }

    /**
     * Entfernt numberOfEdgesToBeRemoved zufällige Edges aus dem Graphen
     *
     * @param rateOfEdgesToBeRemoved
     */
    public void randomlyRemoveEdges(double rateOfEdgesToBeRemoved)
    {
        ArrayList<Edge> edgesArray = new ArrayList<>();
        edgesArray.addAll(this.getSetEdges());
        int numberOfEdgesToBeRemoved = 0;

        if (rateOfEdgesToBeRemoved <= 1)
        {
            if (rateOfEdgesToBeRemoved >= 0)
            {
                numberOfEdgesToBeRemoved = (int) (edgesArray.size() * rateOfEdgesToBeRemoved);
            }
            else
            {
                logger.warn("Weniger als 0% der Kanten sollen gelöscht werden.");
                numberOfEdgesToBeRemoved = 0;
            }
        }
        else
        {
            logger.warn("Ueber 100% der Kanten sollen gelöscht werden.");
            numberOfEdgesToBeRemoved = edgesArray.size();
        }

        for (int i = 0; i < numberOfEdgesToBeRemoved; i++)
        {
            int index = (int) (Math.random() * edgesArray.size());
            this.deleteEdge(edgesArray.get(index));
            edgesArray.remove(index);
        }
    }

    /**
     * Getter Edge-Set
     */
    public HashSet<Edge> getSetEdges()
    {
        return this.graphMap.getSetEdges();
    }

    /**
     * Edge entfernen
     *
     * @param edge
     */
    public void deleteEdge(Edge edge)
    {
        this.graphMap.removeEdge(edge);
    }

    /**
     * Macht nach dem "molecular conjecture" aus einer Edge edgeFactor viele
     *
     * @param edgeFactor
     */
    public void modelMolecularConjecture(int edgeFactor)
    {
        HashSet<Edge> existingEdges = (HashSet<Edge>) this.getSetEdges().clone();

        for (Edge existingEdge : existingEdges)
        {
            for (int i = 0; i < edgeFactor - 1; i++) // eine Edge haben wir ja schon
            {
                Edge newEdge = new Edge(false, true);
                this.addEdge(newEdge, this.getStartNode(existingEdge), this.getEndNode(existingEdge));
            }
        }
    }

    /**
     * Start-Node der Edge ausgeben
     *
     * @param edge
     * @return Start-Node der Edge
     */
    public Node getStartNode(Edge edge)
    {
        return this.graphMap.getKeyByValue(edge).getStartNode();
    }

    /**
     * End-Node der Edge ausgeben
     *
     * @param edge
     * @return End-Node der Edge
     */
    public Node getEndNode(Edge edge)
    {
        return this.graphMap.getKeyByValue(edge).getEndNode();
    }

    /**
     * Entfernt molecularConjecture-Edges
     */
    public void removeMolecularConjectureEdges()
    {
        HashSet<Edge> toBeDeleted = new HashSet<>();

        for (Edge edge : this.getSetEdges())
        {
            if (edge.isMolecularConjecture())
            {
                toBeDeleted.add(edge);
            }
        }
        for (Edge edgeToBeDeleted : toBeDeleted)
        {
            if (edgeToBeDeleted.isCovered())
            {
                this.getStartNode(edgeToBeDeleted).incrPebbles();
            }
            this.deleteEdge(edgeToBeDeleted);
        }
    }

    /**
     * Methode die aus dem aktuellen Graph einen full connected Graph erzeugt
     *
     * @param molecule
     * @param edgeFactor Aus einer chem. Bindung werden edgeFactor Edges erzeugt
     */
    public void changeToFullconnectedGraph(Molekuel molecule, int edgeFactor)
    {
        molecule.getSetAtome();
        Set<Atom> atomMemory = new HashSet<>();
        this.graphMap = new GraphMap();

        for (Atom atom : molecule.getSetAtome())
        {
            for (Atom link : molecule.getSetAtome())
            {
                /* Wenn atom nicht mit sich selbst verbunden ist, und
                 * wenn link noch nicht im memory ist (wenn im Memory, dann Verbindung schon enthalten) */
                if (!atom.equals(link) && !atomMemory.contains(link))
                {
                    double distance = atom.getCoordinates().calcDistance(link.getCoordinates());
                    Node start = new Node(atom.getIdAtom());
                    Node end = new Node(link.getIdAtom());

                    /* Richtung: A -> B */
                    MultiKeyNode multiKey = new MultiKeyNode(start, end);

                    for (int i = 0; i < edgeFactor; i++)
                    {
                        this.graphMap.putEntry(multiKey, new Edge(Edge.count, distance, false, false));
                    }
                }
            }
            atomMemory.add(atom);
        }
    }

    public boolean testFourConnectivity(Node node)
    {
        if (this.getSetEdges(node).size() < 4)
        {
            return true;
        }
        return false;
    }

	/*--------------------------------------------
     *			Setter- und Gettermethoden
	 *------------------------------------------*/

    /**
     * Dreht gegebene Edge um
     * (Edge-Covering: Low-Level (Wrapper))
     *
     * @param edge
     */
    public void changeDirection(Edge edge)
    {
        this.graphMap.changeDirection(edge);
    }

    /**
     * Getter graphMap
     *
     * @return
     */
	public GraphMap getGraphMap()
    {
        return this.graphMap;
    }

    /**
     * Getter Edge-Set by Node
     */
    public HashSet<Edge> getSetEdges(Node node)
    {
        return this.graphMap.getSetEdges(node);
    }

    /**
     * Getter Set redundanter Edges
     */
    public HashSet<Edge> getSetRedundantEdges()
    {
        HashSet<Edge> out = new HashSet<>();

        for (Edge edge : this.getSetEdges())
        {
            if (edge.isRedundant())
            {
                out.add(edge);
            }
        }
        return out;
    }

    /**
     * Getter Set nicht-redundanter Edges
     */
    public HashSet<Edge> getSetNotRedundantEdges()
    {
        HashSet<Edge> out = new HashSet<>();

        for (Edge edge : this.getSetEdges())
        {
            if (!edge.isRedundant())
            {
                out.add(edge);
            }
        }
        return out;
    }

    /**
     * Getter Set nicht-pseudo Edges
     */
    public HashSet<Edge> getSetNonPseudoEdges()
    {
        HashSet<Edge> out = new HashSet<>();

        for (Edge edge : this.getSetEdges())
        {
            if (!edge.isPseudo())
            {
                out.add(edge);
            }
        }
        return out;
    }

    @Override
    public String toString()
    {
        StringBuilder output = new StringBuilder();
        output.append("\n");
        output.append("Graph-ID: " + this.getIdGraph() + "\n");

        int[][] adjacency = new int[this.getSetNodes().size()][this.getSetNodes().size()];

        for (Entry<MultiKeyNode, HashSet<Edge>> entry : this.graphMap.getEntrySet())
        {
            int row = entry.getKey().getStartNode().getIdNode();
            int col = entry.getKey().getEndNode().getIdNode();

            adjacency[row][col] = entry.getValue().size();
        }

        StringBuilder rowString = new StringBuilder();
        for(int row = 0; row < this.getSetNodes().size(); row++)
        {
            for(int col = 0; col < this.getSetNodes().size(); col++)
            {
                rowString.append(adjacency[row][col] + "  ");
            }
            output.append(rowString + "\n");
            rowString = new StringBuilder();
        }

        output.append("\n");

        for (Entry<MultiKeyNode, HashSet<Edge>> entry : this.graphMap.getEntrySet())
        {
            Node startNode = entry.getKey().getStartNode();
            Node endNode = entry.getKey().getEndNode();
            HashSet<Edge> edgeMap = entry.getValue();

            output.append("N1 (" + startNode.getIdNode() + ") - N2 (" + endNode.getIdNode() + "): " + edgeMap.toString() + "\n");
        }

        return output.toString();
    }

    /**
     * Getter Graph-ID
     */
    public String getIdGraph()
    {
        return this.idGraph;
    }

    /**
     * Getter Node-Set
     */
    public HashSet<Node> getSetNodes()
    {
        return this.graphMap.getSetNodes();
    }

    /**
     * Setter Graph-ID
     *
     * @param id Name des Graphen
     */
    public void setIdGraph(String id)
    {
        this.idGraph = id;
    }

}
